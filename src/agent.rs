use std::collections::{
    HashMap,
    HashSet,
};

use serde::{
    Deserialize,
    Serialize,
};
use serde_with::serde_as;

use crate::{
    identity::RandomIdentity,
    message::Type,
    Error,
    Identity,
};

/// The agent is one of the primary building blocks in the Provably Private
/// domain model. It possess type and own constructors for constructing
/// new ones.
#[serde_as]
#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct Agent {
    id: Identity,
    possessions: HashSet<Type>,

    #[serde_as(as = "Vec<(_, _)>")]
    constructors: HashMap<Type, Vec<Type>>,
}

impl Agent {
    /// Creates a new agent with initial possessions and constructors.
    pub fn new(
        possessions: HashSet<Type>,
        constructors: HashMap<Type, Vec<Type>>,
    ) -> Agent {
        Agent {
            id: RandomIdentity::new(),
            possessions,
            constructors,
        }
    }

    /// Creates a new agent without any constructors or possessions.
    pub fn empty(id: Identity) -> Agent {
        Agent {
            id,
            possessions: HashSet::new(),
            constructors: HashMap::new(),
        }
    }

    /// Adds a constructor.
    pub fn add_constructor(&mut self, constructor: (Type, Vec<Type>)) {
        let (result, args) = constructor;
        self.constructors.insert(result, args);
    }

    /// Agents id.
    pub fn id(&self) -> &Identity { &self.id }

    /// Gets the agents unique identifier.
    pub fn clone_id(&self) -> Identity { self.id.clone() }

    /// Gets the agent constructors.
    pub fn get_constructors(&self) -> HashMap<Type, Vec<Type>> {
        self.constructors.clone()
    }

    pub fn constructors_borrow(&self) -> &HashMap<Type, Vec<Type>> {
        &self.constructors
    }

    /// Returns a bool indicating whether or not an agent possess a certain
    /// type.
    pub fn possesses(&self, _type: &Type) -> bool {
        self.intermediate_args(_type).is_ok()
    }

    pub fn get_possesions_clone(&self) -> HashSet<Type> {
        self.possessions.clone()
    }

    /// Lets an agent gain possession over the given type.
    pub fn acquire(&mut self, _type: Type) { self.possessions.insert(_type); }

    /// Checks whether or not the agent have possession over a given type
    /// or if it is able to construct it with its current possessions and
    /// constructors.
    ///
    /// If the agent is not able to construct the type it will result in
    /// an error.
    ///
    /// # Examples
    ///
    /// ## Recursive construct
    /// The construction of type work in a recursive manner. That means
    /// that the following code will not result in error.
    /// ```    
    /// use std::collections::HashSet;
    /// use std::collections::HashMap;
    ///
    /// use ppc::agent::Agent;
    /// use ppc::Type;
    ///
    /// let mut constructors = HashMap::new();
    /// constructors.insert(Type::atomic("A"), vec![]);
    /// constructors.insert(Type::atomic("B"), vec![Type::atomic("A")]);
    /// constructors.insert(Type::atomic("C"), vec![Type::atomic("B")]);
    ///
    /// let mut agent = Agent::new(HashSet::new(), constructors);
    ///
    /// // Before this call agent possesses nothing.
    /// agent.construct(&Type::atomic("C")).unwrap();
    ///     
    /// // agent now posses everything needed to construct the atomic type C.
    /// assert!(agent.possesses(&Type::atomic("A")));
    /// assert!(agent.possesses(&Type::atomic("B")));
    /// assert!(agent.possesses(&Type::atomic("C")));
    /// ```
    pub fn construct(&mut self, _type: &Type) -> Result<(), Error> {
        let _types = self.intermediate_args(_type)?;

        for _type in _types {
            self.possessions.insert(_type);
        }

        Ok(())
    }

    /// Helper function that checks if an agent has constructors and possession
    /// of type required to create the given type.
    ///
    /// On success a collection of all type not in the agents possession set
    /// but needed to construct the given type is returned.
    ///
    /// # Note
    ///
    /// See the function `Agent::construct` to get a better understanding of the
    /// purpose of this function.
    fn intermediate_args(&self, _type: &Type) -> Result<Vec<Type>, Error> {
        if self.possessions.contains(_type) {
            Ok(Vec::new())
        } else {
            guard!(let Some(args) = self.constructors.get(_type) else {
                return Err(Error::Agent(AgentError::MissingPossession))
            });

            args.iter().map(|arg| self.intermediate_args(arg)).try_fold(
                vec![_type.clone()],
                |mut res, args| match args {
                    Err(e) => Err(e),
                    Ok(args) => {
                        res.append(&mut args.clone());
                        Ok(res)
                    }
                },
            )
        }
    }
}

/// Errors related to Agent
#[derive(Debug)]
pub enum AgentError {
    MissingPossession,
}

/// Builds constructors.
pub struct ConstructorBuilder(Option<Type>, Vec<Type>);

impl ConstructorBuilder {
    pub fn new() -> ConstructorBuilder { ConstructorBuilder(None, Vec::new()) }

    /// Set the result type of the constructor.
    pub fn result(mut self, _type: Type) -> ConstructorBuilder {
        self.0 = Some(_type);
        self
    }

    /// Add argument to the constructor.
    pub fn arg(mut self, _type: Type) -> ConstructorBuilder {
        self.1.push(_type);
        self
    }

    /// Build the constructor. Panics if result type is missing.
    pub fn build(self) -> Result<(Type, Vec<Type>), &'static str> {
        if let Some(result) = self.0 {
            Ok((result, self.1))
        } else {
            Err("missing result type")
        }
    }
}

/// agent macro uses a simple DSL to describe agent. Returns an [Agent]
/// # Syntax
/// Comma seperated lines like this:
/// [List of [Type] seperated by commas] => [[Type]]
/// for each constructor.
/// For no constructors provide nothing.
/// # Examples
/// ## Multiple constructors
/// ```
///    use ppc::*;
///    use ppc::agent::Agent;
///    use ppc::Type;
///    use ppc::identity::Identity;
///
///    agent!(
///        Type::certificate(&Identity::from("Website"), "CONSENT")
///        => Type::atomic("CONSENT"),
///        Type::certificate(&Identity::from("Website"), "INFO")
///        => Type::atomic("INFO")
///    );
/// ```
/// ## One constructor
/// ```
///    use ppc::*;
///    use ppc::agent::Agent;
///    use ppc::Type;
///    use ppc::identity::Identity;
///
///    agent!(
///        Type::atomic("INFO"),
///        Type::proof(&Identity::from("Website"), "CONSENT")
///        => Type::certificate(&Identity::from("Website"), "INFO")
///    );
/// ```
/// ## No constructor
/// ```
/// use ppc::agent::Agent;
/// use ppc::*;
/// agent!();
/// ```
#[macro_export]
macro_rules! agent {
    () => {{
        let possessions = std::collections::HashSet::new();
        let constructors = std::collections::HashMap::new();
        Agent::new(possessions, constructors)
    }};
    ($($($x:expr),* => $y:expr),+) => {{
        let possessions = std::collections::HashSet::new();
        let mut constructors = std::collections::HashMap::new();
        $(
            constructors.insert($y, vec![$($x),*]);
        )+
        Agent::new(possessions, constructors)
    }};
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn json_serialize_and_deserialize() {
        let mut agent = agent!(
            => Type::atomic("A"),
            => Type::proof(&Identity::from("alpha"), "A"),
            => Type::certificate(&Identity::from("alpha"), "A"),

            Type::atomic("A"),
            Type::proof(&Identity::from("alpha"), "A")
            => Type::certificate(&Identity::from("beta"), "B")
        );
        agent.acquire(Type::atomic("X"));
        agent.acquire(Type::proof(&Identity::from("gamma"), "Y"));
        agent.acquire(Type::certificate(&Identity::from("delta"), "Z"));

        let ser = serde_json::to_string(&agent).unwrap();
        let de: Agent = serde_json::from_str(&ser).unwrap();

        assert_eq!(agent, de);
    }

    /// Testing that the function `Agent::intermediate_args` is working for
    /// an agent looking like this:
    ///
    /// Possessions:
    ///   A0
    ///   A1        
    /// Constructors:
    ///   A0 => B
    ///   A1 => C
    ///   A0 => A1 => D
    ///   B  => C  => E
    #[test]
    fn intermediate_args_working() {
        let mut agent = agent!(
            Type::atomic("A0") => Type::atomic("B"),
            Type::atomic("A1") => Type::atomic("C"),
            Type::atomic("A0"), Type::atomic("A1") => Type::atomic("D"),
            Type::atomic("B"), Type::atomic("C") => Type::atomic("E")
        );
        agent.acquire(Type::atomic("A0"));
        agent.acquire(Type::atomic("A1"));

        // Construction of possessed type. No intermediate arguments.

        assert_eq!(
            agent.intermediate_args(&Type::atomic("A0")).unwrap(),
            vec![]
        );

        assert_eq!(
            agent.intermediate_args(&Type::atomic("A1")).unwrap(),
            vec![]
        );

        // Construction of  where agent have possession over all
        // arguments for its constructor. Intermediate arguments should only be
        // the constructed .

        assert_eq!(
            agent.intermediate_args(&Type::atomic("B")).unwrap(),
            vec![Type::atomic("B")]
        );

        assert_eq!(
            agent.intermediate_args(&Type::atomic("C")).unwrap(),
            vec![Type::atomic("C")]
        );

        assert_eq!(
            agent.intermediate_args(&Type::atomic("D")).unwrap(),
            vec![Type::atomic("D")]
        );

        // Multiple intermediate arguments. Using sets since order should not
        // matter when comparing.

        let result: HashSet<Type> = agent
            .intermediate_args(&Type::atomic("E"))
            .unwrap()
            .into_iter()
            .collect();
        let expect: HashSet<Type> =
            vec![Type::atomic("B"), Type::atomic("C"), Type::atomic("E")]
                .into_iter()
                .collect();

        assert_eq!(result, expect);
    }

    #[test]
    fn intermediate_args_error() {
        let agent = agent!(
            => Type::atomic("A"),
            Type::atomic("A") => Type::atomic("B"),
            Type::atomic("C") => Type::atomic("D"),
            Type::atomic("B"), Type::atomic("D") => Type::atomic("E")
        );

        for x in ["X", "C", "D", "E"].iter() {
            agent
                .intermediate_args(&Type::atomic(x))
                .expect_err("agent should not be able to construct type");
        }
    }

    #[test]
    fn test_agent() {
        let possessions = HashSet::new();
        let constructors = HashMap::new();
        let empty_agent = Agent::new(possessions, constructors);
        // Test empty agent
        assert_eq!(empty_agent.possessions, agent!().possessions);
        assert_eq!(empty_agent.constructors, agent!().constructors);

        let possessions = HashSet::new();
        let mut constructors = HashMap::new();
        constructors.insert(
            Type::certificate(&Identity::from("Website"), "INFO"),
            vec![
                Type::atomic("INFO"),
                Type::proof(&Identity::from("Website"), "CONSENT"),
            ],
        );
        let one_constructor_agent = Agent::new(possessions, constructors);
        let macro_one_constructor_agent = agent!(
            Type::atomic("INFO"),
            Type::proof(&Identity::from("Website"), "CONSENT")
            => Type::certificate(&Identity::from("Website"), "INFO")
        );

        // Test mulitple type required in one constructor
        assert_eq!(
            one_constructor_agent.possessions,
            macro_one_constructor_agent.possessions
        );
        assert_eq!(
            one_constructor_agent.constructors,
            macro_one_constructor_agent.constructors
        );

        let possessions = HashSet::new();
        let mut constructors = HashMap::new();
        constructors.insert(Type::atomic("INFO"), vec![]);
        constructors.insert(
            Type::certificate(&Identity::from("Website"), "CONSENT"),
            vec![Type::atomic("CONSENT")],
        );
        let multiple_constructor_agent = Agent::new(possessions, constructors);
        let macro_multiple_constructor_agent = agent!(
            => Type::atomic("INFO"),
            Type::atomic("CONSENT") => Type::certificate(&Identity::from("Website"), "CONSENT")
        );

        // Test multiple constructors, where one has no args
        assert_eq!(
            multiple_constructor_agent.possessions,
            macro_multiple_constructor_agent.possessions
        );
        assert_eq!(
            multiple_constructor_agent.constructors,
            macro_multiple_constructor_agent.constructors
        );
    }

    #[test]
    fn constructor_builder_works() {
        ConstructorBuilder::new()
            .build()
            .expect_err("should fail when missing result type");

        let constructor = ConstructorBuilder::new()
            .result(Type::atomic("A"))
            .build()
            .unwrap();
        assert_eq!(constructor, (Type::atomic("A"), Vec::new()));

        let constructor = ConstructorBuilder::new()
            .arg(Type::atomic("B"))
            .arg(Type::atomic("C"))
            .result(Type::atomic("A"))
            .build()
            .unwrap();

        assert_eq!(
            constructor,
            (
                Type::atomic("A"),
                vec![Type::atomic("B"), Type::atomic("C")]
            )
        );
    }
}
