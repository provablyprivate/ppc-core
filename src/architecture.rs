use std::collections::HashMap;

use serde::{
    Deserialize,
    Serialize,
};

use crate::{
    Agent,
    Identity,
};

/// Stores all agents.
#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct Architecture {
    agents: HashMap<Identity, Agent>,
    input_interfaces: HashMap<Identity, Agent>,
    output_interfaces: HashMap<Identity, Agent>,
}

impl Architecture {
    pub fn new() -> Architecture {
        Architecture {
            agents: HashMap::new(),
            input_interfaces: HashMap::new(),
            output_interfaces: HashMap::new(),
        }
    }

    /// Insert an agent for the given key in the architecture.
    /// Returns the ID if needed for sending messages
    pub fn insert_agent(&mut self, agent: Agent) -> Identity {
        let id = agent.clone_id();
        self.agents.insert(id.clone(), agent);
        id
    }

    /// Insert an input interface for the given key in the architecture.
    /// Returns the ID if needed for sending messages
    pub fn insert_input_interface(&mut self, agent: Agent) -> Identity {
        let id = agent.clone_id();
        self.input_interfaces.insert(id.clone(), agent);
        id
    }

    /// Insert an output interface for the given key in the architecture.
    /// Returns the ID if needed for sending messages
    pub fn insert_output_interface(&mut self, agent: Agent) -> Identity {
        let id = agent.clone_id();
        self.output_interfaces.insert(id.clone(), agent);
        id
    }

    /// Gets all agents in the architecture.
    pub fn get_agents(&self) -> HashMap<Identity, Agent> { self.agents.clone() }

    /// Gets all input interfaces in the architecture.
    pub fn get_input_interfaces(&self) -> HashMap<Identity, Agent> {
        self.input_interfaces.clone()
    }

    /// Gets all ouput interfaces in the architecture.
    pub fn get_output_interfaces(&self) -> HashMap<Identity, Agent> {
        self.output_interfaces.clone()
    }

    /// Borrows the agent associated with `id`.
    pub fn agent_borrow(
        &self,
        id: &Identity,
    ) -> Option<(&Agent, &Agent, &Agent)> {
        let base_agent = self.agents.get(id)?;
        let input_agent = self.input_interfaces.get(id)?;
        let output_agent = self.output_interfaces.get(id)?;
        Some((base_agent, input_agent, output_agent))
    }

    pub fn get_agents_borrow_mut(&mut self) -> &mut HashMap<Identity, Agent> {
        &mut self.agents
    }
}

/// Errors raised by `Architecture`.
///
/// ## NOTE:
/// There currently are no architecture errors.
#[derive(Debug)]
pub enum ArchitectureError {}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::Type;

    #[test]
    fn inserted_id_same_as_original() {
        let ag = agent!();
        let id_orig = ag.clone_id();
        let mut architecture = Architecture::new();

        let id_inserted = architecture.insert_agent(ag);

        assert_eq!(id_orig, id_inserted);
    }

    #[test]
    fn serialize_deserialize() {
        // Create agent alpha.
        let mut alpha = agent!(
            => Type::atomic("A"),
            => Type::proof(&Identity::from("alpha"), "A"),
            => Type::certificate(&Identity::from("alpha"), "A"),

            Type::atomic("A"),
            Type::proof(&Identity::from("alpha"), "A")
            => Type::certificate(&Identity::from("beta"), "B")
        );
        alpha.acquire(Type::atomic("X"));
        alpha.acquire(Type::proof(&Identity::from("gamma"), "Y"));
        alpha.acquire(Type::certificate(&Identity::from("delta"), "Z"));

        // Create agent beta.
        let mut beta = agent!(
            Type::atomic("A0") => Type::atomic("B"),
            Type::atomic("A1") => Type::atomic("C"),
            Type::atomic("A0"), Type::atomic("A1") => Type::atomic("D"),
            Type::atomic("B"), Type::atomic("C") => Type::atomic("E")
        );
        beta.acquire(Type::atomic("A0"));
        beta.acquire(Type::atomic("A1"));

        // Create architecture and insert agents.
        let mut architecture = Architecture::new();
        architecture.insert_agent(alpha);
        architecture.insert_input_interface(beta.clone());
        architecture.insert_output_interface(beta);

        // Serialize and deserialize architecture.
        let ser = serde_json::to_string(&architecture).unwrap();
        let de = serde_json::from_str(&ser).unwrap();

        assert_eq!(architecture, de);
    }
}
