use std::collections::HashMap;
use std::error::Error;
use std::fmt;

use crate::{
    Agent,
    Identity,
    Keyring,
};

pub struct KeyCollection {
    my_id: Identity,
    owned_keys: (Vec<u8>, Vec<u8>),
    agent_encryption_keys: HashMap<Identity, Vec<u8>>,
    agent_sign_keys: HashMap<Identity, Vec<u8>>,
}

impl KeyCollection {
    pub fn new(
        id: Identity,
        secret_sign: Vec<u8>,
        secret_encrypt: Vec<u8>,
    ) -> KeyCollection {
        Self {
            my_id: id,
            owned_keys: (secret_sign, secret_encrypt),
            agent_encryption_keys: HashMap::new(),
            agent_sign_keys: HashMap::new(),
        }
    }

    pub fn insert_agent(
        &mut self,
        agent: Agent,
        enc_key: Vec<u8>,
        sign_key: Vec<u8>,
    ) {
        self.agent_encryption_keys.insert(agent.clone_id(), enc_key);
        self.agent_sign_keys.insert(agent.clone_id(), sign_key);
    }
}

#[derive(Debug, Clone)]
pub struct KeyCollectionError {}

impl fmt::Display for KeyCollectionError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "KeyColletion Failed. Make Sure you own the neccessary Keys"
        )
    }
}

impl Error for KeyCollectionError {}

impl Keyring for KeyCollection {
    type Err = KeyCollectionError;

    // I do not think we need to send a refernce to an identity since we will
    // only have access to one pair of private keys from the quartette
    fn secret_sign_bytes(&self, id: &Identity) -> Result<&[u8], Self::Err> {
        if self.my_id == id.clone() {
            Ok(self.owned_keys.0.as_slice())
        } else {
            Err(KeyCollectionError {})
        }
    }

    fn public_sign_bytes(&self, id: &Identity) -> Result<&[u8], Self::Err> {
        match self.agent_sign_keys.get(id) {
            None => Err(KeyCollectionError {}),
            Some(result) => Ok(result),
        }
    }

    fn secret_encryption_bytes(
        &self,
        id: &Identity,
    ) -> Result<&[u8], Self::Err> {
        if self.my_id == id.clone() {
            Ok(self.owned_keys.1.as_slice())
        } else {
            Err(KeyCollectionError {})
        }
    }

    fn public_encryption_bytes(
        &self,
        id: &Identity,
    ) -> Result<&[u8], Self::Err> {
        match self.agent_encryption_keys.get(id) {
            None => Err(KeyCollectionError {}),
            Some(result) => Ok(result),
        }
    }
}
