use std::collections::HashSet;

use crate::Architecture;
use crate::Type;
use crate::{
    abstract_syntax::abstract_syntax::ASPositiveConstraint,
    Agent,
};

/// This function confirms that the positive constraints in a architecture are
/// valid
pub fn validate_architecture(
    positive_constraints: Vec<ASPositiveConstraint>,
    architecture: Architecture,
) -> Result<Architecture, ()> {
    match positive_constraints
        .iter()
        .map(|(identity, _type)| {
            recursive_constraint_checker(
                identity,
                _type,
                &architecture,
                &mut HashSet::new(),
                &AgentType::Input,
            )
        })
        .all(|result| result == true)
    {
        true => Ok(architecture),
        false => Err(()),
    }
}

// Helper functions for the constraint checker as we don't have any
// input/output/agent logic.
#[derive(PartialEq, Debug)]
enum AgentType {
    Agent,
    Input,
    Output,
}

// Recursively check if an item can be produced.
// To avoid cycles we have a HashSet<(Agent, Constructor)> that contains already
// tried paths
fn recursive_constraint_checker(
    identity: &String,
    _type: &Type,
    architecture: &Architecture,
    visited: &HashSet<(String, Type)>,
    agent_type: &AgentType,
) -> bool {
    let agents = architecture.get_agents();
    let inputs = architecture.get_input_interfaces();
    let outputs = architecture.get_output_interfaces();
    let agent = match agent_type {
        AgentType::Agent => agents.get(identity).unwrap(),
        AgentType::Input => inputs.get(identity).unwrap(),
        AgentType::Output => outputs.get(identity).unwrap(),
    };
    match agent.get_constructors().get(&_type) {
        Some(args) => match &args[..] {
            // Needs nothing to produce, valid trace found, return true
            [] => true,
            // Args needed to produce, recursively check if they can be produced
            _ => {
                // For each item in args, can it be produced by any other agent
                // of the correct type
                args.iter()
                    .map(|arg| {
                        let producers = match agent_type {
                            // If agent type is Input then we can look at
                            // all output agents
                            AgentType::Input => outputs
                                .values()
                                .map(|agent| (agent, AgentType::Output))
                                .filter(|(agent, _)| {
                                    agent.get_constructors().contains_key(arg)
                                })
                                .collect::<Vec<(&Agent, AgentType)>>(),
                            // If agent type is agent it can only look at
                            // its input
                            AgentType::Agent => {
                                vec![(
                                    inputs.get(agent.id()).unwrap(),
                                    AgentType::Input,
                                )]
                            }
                            // If agent type is output it can only look at
                            // its own agent, its own input and all the outputs
                            AgentType::Output => outputs
                                .values()
                                .map(|agent| (agent, AgentType::Output))
                                .chain(vec![
                                    // Its own agent
                                    (
                                        agents.get(agent.id()).unwrap(),
                                        AgentType::Agent,
                                    ),
                                    // Its own input
                                    (
                                        inputs.get(agent.id()).unwrap(),
                                        AgentType::Input,
                                    ),
                                ])
                                .filter(|(agent, _)| {
                                    agent.get_constructors().contains_key(arg)
                                })
                                .collect::<Vec<(&Agent, AgentType)>>(),
                        };
                        // For these producers, is there a valid trace leading
                        // to that production?
                        producers
                            .iter()
                            .map(|(agent, agent_type)| {
                                // If already checked, abandon
                                let visit = (agent.clone_id(), arg.clone());
                                if visited.contains(&visit) {
                                    return false;
                                } else {
                                    let mut new_visited = visited.clone();
                                    new_visited.insert(visit);
                                    recursive_constraint_checker(
                                        agent.id(),
                                        arg,
                                        architecture,
                                        &new_visited,
                                        agent_type,
                                    )
                                }
                            })
                            // We only need one valid trace for the positive
                            // constraint to hold
                            .any(|b| b == true)
                    })
                    // All the requried args need to be able to be produced
                    .all(|b| b == true)
            }
        },
        // This should not happen, means agent can not produce item
        None => {
            println!("Agent type: {:?}, Identity: {:?}", agent_type, identity);
            panic!(
                "Validator tried to investigate agent for type it can not \
                 produce!"
            )
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::Agent;
    use crate::Type;

    // Helper function for sample architecture with negative constraint
    // Agent1 - Item2 => Agent2 - Item1
    fn architecture_valid() -> Architecture {
        let mut agent1 = Agent::empty("Agent1".to_string());
        let mut agent2 = Agent::empty("Agent2".to_string());
        let agent3 = Agent::empty("Agent3".to_string());
        let mut i_agent1 = Agent::empty("Agent1".to_string());
        let mut o_agent1 = Agent::empty("Agent1".to_string());
        let mut i_agent2 = Agent::empty("Agent2".to_string());
        let mut o_agent2 = Agent::empty("Agent2".to_string());
        let mut i_agent3 = Agent::empty("Agent3".to_string());
        let mut o_agent3 = Agent::empty("Agent3".to_string());

        let item1 = Type::atomic(&"Item1".to_string());
        let ca1item1 =
            Type::certificate(&agent1.id(), &item1.get_type_string());
        let pa1item1 = Type::proof(&agent1.id(), &item1.get_type_string());
        let ca2item1 =
            Type::certificate(&agent2.id(), &item1.get_type_string());
        let pa2item1 = Type::proof(&agent2.id(), &item1.get_type_string());
        let ca3item1 =
            Type::certificate(&agent3.id(), &item1.get_type_string());
        let pa3item1 = Type::proof(&agent3.id(), &item1.get_type_string());

        let item2 = Type::atomic(&"Item2".to_string());
        let ca1item2 =
            Type::certificate(&agent1.id(), &item2.get_type_string());
        let pa1item2 = Type::proof(&agent1.id(), &item2.get_type_string());
        let ca2item2 =
            Type::certificate(&agent2.id(), &item2.get_type_string());
        let pa2item2 = Type::proof(&agent2.id(), &item2.get_type_string());
        let ca3item2 =
            Type::certificate(&agent3.id(), &item2.get_type_string());
        let pa3item2 = Type::proof(&agent3.id(), &item2.get_type_string());

        agent1.add_constructor((item1.clone(), vec![]));
        agent2.add_constructor((item2.clone(), vec![]));

        i_agent1.add_constructor((item2.clone(), vec![ca1item2.clone()]));
        i_agent1.add_constructor((item1.clone(), vec![ca1item1.clone()]));
        o_agent1.add_constructor((pa1item1.clone(), vec![item1.clone()]));
        o_agent1.add_constructor((pa1item2.clone(), vec![item2.clone()]));
        o_agent1.add_constructor((ca1item1.clone(), vec![item1.clone()]));
        o_agent1.add_constructor((
            ca1item2.clone(),
            vec![item2.clone(), pa3item2.clone()],
        ));
        o_agent1.add_constructor((ca2item1.clone(), vec![item1.clone()]));
        o_agent1.add_constructor((ca2item2.clone(), vec![item2.clone()]));
        o_agent1.add_constructor((ca3item1.clone(), vec![item1.clone()]));
        o_agent1.add_constructor((
            ca3item2.clone(),
            vec![item2.clone(), pa2item1.clone()],
        ));

        i_agent2.add_constructor((item1.clone(), vec![ca2item1.clone()]));
        i_agent2.add_constructor((item2.clone(), vec![ca2item2.clone()]));
        o_agent2.add_constructor((pa2item2.clone(), vec![item2.clone()]));
        o_agent2.add_constructor((pa2item1.clone(), vec![item1.clone()]));
        o_agent2.add_constructor((ca1item1.clone(), vec![item1.clone()]));
        o_agent2.add_constructor((
            ca1item2.clone(),
            vec![item2.clone(), pa3item2.clone()],
        ));
        o_agent2.add_constructor((ca2item2.clone(), vec![item2.clone()]));
        o_agent2.add_constructor((ca2item1.clone(), vec![item1.clone()]));
        o_agent2.add_constructor((
            ca3item2.clone(),
            vec![item2.clone(), pa2item1.clone()],
        ));
        o_agent2.add_constructor((ca3item1.clone(), vec![item1.clone()]));

        i_agent3.add_constructor((item1.clone(), vec![ca3item1.clone()]));
        i_agent3.add_constructor((item2.clone(), vec![ca3item2.clone()]));
        o_agent3.add_constructor((pa3item2.clone(), vec![item2.clone()]));
        o_agent3.add_constructor((pa3item1.clone(), vec![item1.clone()]));
        o_agent3.add_constructor((ca1item1.clone(), vec![item1.clone()]));
        o_agent3.add_constructor((
            ca1item2.clone(),
            vec![item2.clone(), pa3item2.clone()],
        ));
        o_agent3.add_constructor((ca2item2.clone(), vec![item2.clone()]));
        o_agent3.add_constructor((ca2item1.clone(), vec![item1.clone()]));
        o_agent3.add_constructor((
            ca3item2.clone(),
            vec![item2.clone(), pa2item1.clone()],
        ));
        o_agent3.add_constructor((ca3item1.clone(), vec![item1.clone()]));

        let mut manual_ex_architecture = Architecture::new();
        manual_ex_architecture.insert_agent(agent1);
        manual_ex_architecture.insert_agent(agent2);
        manual_ex_architecture.insert_agent(agent3);
        manual_ex_architecture.insert_input_interface(i_agent1);
        manual_ex_architecture.insert_output_interface(o_agent1);
        manual_ex_architecture.insert_input_interface(i_agent2);
        manual_ex_architecture.insert_output_interface(o_agent2);
        manual_ex_architecture.insert_input_interface(i_agent3);
        manual_ex_architecture.insert_output_interface(o_agent3);
        manual_ex_architecture
    }

    // Agent 1 - Item 2 => Agent 2 - Item 1
    // Agent 2 - Item 1 => Agent 3 - Item 2
    // Agent 3 - Item 2 => Agent 1 - Item 2
    fn architecture_invalid() -> Architecture {
        let mut manual_ex_architecture = Architecture::new();
        let mut agent1 = Agent::empty("Agent1".to_string());
        let mut agent2 = Agent::empty("Agent2".to_string());
        let agent3 = Agent::empty("Agent3".to_string());
        let mut i_agent1 = Agent::empty("Agent1".to_string());
        let mut o_agent1 = Agent::empty("Agent1".to_string());
        let mut i_agent2 = Agent::empty("Agent2".to_string());
        let mut o_agent2 = Agent::empty("Agent2".to_string());
        let mut i_agent3 = Agent::empty("Agent3".to_string());
        let mut o_agent3 = Agent::empty("Agent3".to_string());

        let item1 = Type::atomic(&"Item1".to_string());
        let item2 = Type::atomic(&"Item2".to_string());

        agent1.add_constructor((item1.clone(), vec![]));
        agent2.add_constructor((item2.clone(), vec![]));

        let cert_agent3_item2 =
            Type::certificate(agent3.id(), &item2.get_type_string());
        let cert_agent3_item1 =
            Type::certificate(agent3.id(), &item1.get_type_string());
        i_agent3
            .add_constructor((item2.clone(), vec![cert_agent3_item2.clone()]));
        i_agent3
            .add_constructor((item1.clone(), vec![cert_agent3_item1.clone()]));

        let cert_agent2_item2 =
            Type::certificate(agent2.id(), &item2.get_type_string());
        let cert_agent2_item1 =
            Type::certificate(agent2.id(), &item1.get_type_string());
        i_agent2
            .add_constructor((item2.clone(), vec![cert_agent2_item2.clone()]));
        i_agent2
            .add_constructor((item1.clone(), vec![cert_agent2_item1.clone()]));

        let cert_agent1_item2 =
            Type::certificate(agent1.id(), &item2.get_type_string());
        let cert_agent1_item1 =
            Type::certificate(agent1.id(), &item1.get_type_string());
        i_agent1
            .add_constructor((item2.clone(), vec![cert_agent1_item2.clone()]));
        i_agent1
            .add_constructor((item1.clone(), vec![cert_agent1_item1.clone()]));

        let proof_agent1_item1 =
            Type::proof(agent1.id(), &item1.get_type_string());
        let proof_agent1_item2 =
            Type::proof(agent1.id(), &item2.get_type_string());
        let proof_agent2_item1 =
            Type::proof(agent2.id(), &item1.get_type_string());
        let proof_agent2_item2 =
            Type::proof(agent2.id(), &item2.get_type_string());
        let proof_agent3_item2 =
            Type::proof(agent3.id(), &item2.get_type_string());

        o_agent1
            .add_constructor((cert_agent1_item1.clone(), vec![item1.clone()]));
        o_agent1.add_constructor((
            cert_agent1_item2.clone(),
            vec![item2.clone(), proof_agent2_item1.clone()],
        ));
        o_agent1.add_constructor((
            cert_agent2_item1.clone(),
            vec![item1.clone(), proof_agent3_item2.clone()],
        ));
        o_agent1
            .add_constructor((cert_agent2_item2.clone(), vec![item2.clone()]));
        o_agent1
            .add_constructor((cert_agent3_item1.clone(), vec![item1.clone()]));
        o_agent1.add_constructor((
            cert_agent3_item2.clone(),
            vec![item2.clone(), proof_agent1_item2.clone()],
        ));
        o_agent1
            .add_constructor((proof_agent1_item1.clone(), vec![item1.clone()]));
        o_agent1
            .add_constructor((proof_agent1_item2.clone(), vec![item2.clone()]));

        o_agent2
            .add_constructor((cert_agent1_item1.clone(), vec![item1.clone()]));
        o_agent2.add_constructor((
            cert_agent1_item2.clone(),
            vec![item2.clone(), proof_agent2_item1.clone()],
        ));
        o_agent2.add_constructor((
            cert_agent2_item1.clone(),
            vec![item1.clone(), proof_agent3_item2.clone()],
        ));
        o_agent2
            .add_constructor((cert_agent2_item2.clone(), vec![item2.clone()]));
        o_agent2.add_constructor((
            cert_agent3_item2.clone(),
            vec![item2.clone(), proof_agent1_item2.clone()],
        ));
        o_agent2
            .add_constructor((cert_agent3_item1.clone(), vec![item1.clone()]));
        o_agent2
            .add_constructor((proof_agent2_item1.clone(), vec![item1.clone()]));
        o_agent2
            .add_constructor((proof_agent2_item2.clone(), vec![item2.clone()]));

        o_agent3
            .add_constructor((cert_agent3_item1.clone(), vec![item1.clone()]));
        o_agent3.add_constructor((
            cert_agent2_item1.clone(),
            vec![item1.clone(), proof_agent3_item2.clone()],
        ));
        o_agent3
            .add_constructor((cert_agent2_item2.clone(), vec![item2.clone()]));
        o_agent3
            .add_constructor((cert_agent3_item1.clone(), vec![item1.clone()]));
        o_agent3.add_constructor((
            cert_agent1_item2.clone(),
            vec![item2.clone(), proof_agent2_item1.clone()],
        ));
        o_agent3.add_constructor((
            cert_agent3_item1,
            vec![item1.clone(), proof_agent1_item2.clone()],
        ));
        o_agent3.add_constructor((
            proof_agent3_item2,
            vec![item2.clone(), proof_agent1_item2.clone()],
        ));
        o_agent3.add_constructor((cert_agent1_item1, vec![item1.clone()]));

        manual_ex_architecture.insert_agent(agent1.clone());
        manual_ex_architecture.insert_agent(agent2.clone());
        manual_ex_architecture.insert_agent(agent3.clone());
        manual_ex_architecture.insert_input_interface(i_agent1.clone());
        manual_ex_architecture.insert_output_interface(o_agent1.clone());
        manual_ex_architecture.insert_input_interface(i_agent2.clone());
        manual_ex_architecture.insert_output_interface(o_agent2.clone());
        manual_ex_architecture.insert_input_interface(i_agent3.clone());
        manual_ex_architecture.insert_output_interface(o_agent3.clone());

        manual_ex_architecture
    }

    // Valid positive constraint should succeed, Agent 1 can receive Item 2
    #[test]
    fn test_valid_architecture() {
        let positive_constraints = vec![
            (String::from("Agent1"), Type::atomic("Item2")),
            (String::from("Agent2"), Type::atomic("Item1")),
            (String::from("Agent3"), Type::atomic("Item2")),
        ];

        assert!(validate_architecture(
            positive_constraints,
            architecture_valid()
        )
        .is_ok());
    }

    // Invalid positive cosntraint should fail, Agent1 can never receive Item 3
    // Need to find a more complicated case that can fail
    #[test]
    fn test_invalid_architecture() {
        let positive_constraints =
            vec![(String::from("Agent2"), Type::atomic("Item1"))];

        assert!(!validate_architecture(
            positive_constraints,
            architecture_invalid()
        )
        .is_ok());
    }
}
