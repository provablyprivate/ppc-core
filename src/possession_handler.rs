use std::{
    collections::HashMap,
    error::Error,
};

use crate::{
    message::MessageError,
    Identity,
    Keyring,
    Message,
    Type,
};

pub trait PossessionHandler {
    type Err: Error + 'static;
    type Body: Clone;

    fn query_possession(
        &self,
        keyring: &impl Keyring,
        _type: &Type,
        receiver: &Identity,
    ) -> Result<Option<Message<Self::Body>>, Self::Err>;

    fn insert_possession<K>(
        &mut self,
        keyring: &K,
        message: Message<Self::Body>,
    ) -> Result<(), Self::Err>
    where
        K: Keyring,
        K::Err: 'static;
}

// -----------------------------------------------------------------------------
// Implementations
// -----------------------------------------------------------------------------

/// `HashMap<Type, Message<T>>` can be used as a `PossessionHandler`.
impl<T> PossessionHandler for HashMap<Type, Vec<Message<T>>>
where
    T: Clone,
{
    type Err = MessageError;
    type Body = T;

    fn query_possession(
        &self,
        keyring: &impl Keyring,
        _type: &Type,
        receiver: &Identity,
    ) -> Result<Option<Message<T>>, Self::Err> {
        if let Some(messages) = self.get(_type) {
            for message in messages.iter() {
                if message.receiver(keyring)? == receiver {
                    return Ok(Some(message.clone()));
                }
            }
            Ok(None)
        } else {
            Ok(None)
        }
    }

    fn insert_possession<K>(
        &mut self,
        keyring: &K,
        message: Message<Self::Body>,
    ) -> Result<(), Self::Err>
    where
        K: Keyring,
        K::Err: 'static,
    {
        let key = message.term(keyring)?._type();

        if let Some(messages) = self.get_mut(key) {
            messages.push(message);
        } else {
            self.insert(key.clone(), vec![message]);
        }
        Ok(())
    }
}
