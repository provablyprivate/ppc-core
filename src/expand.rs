use std::collections::HashMap;

use crate::Architecture;
use crate::Identity;
use crate::Type;
use crate::{
    validate::validate_architecture,
    Agent,
};

pub type NegativeConstraint = (Identity, Type, Identity, Type);
pub type PositiveConstraint = (Identity, Type);

///Expands a architecture into a safe architecture, this is done by adding
/// interface agents and applying negative constraints onto the architecture.
//Currently there is no verification of positive constraints, which would need
// to verify that a valid trace can be found where the result is the agent in
// the positive constraint acquires the item in the constraint.
pub fn expand_architecture(
    mut architecture: Architecture,
    negative_constraints: Vec<NegativeConstraint>,
    positive_constraints: Vec<PositiveConstraint>,
) -> Architecture {
    //Composes a list of all possible items which can be constructed.
    let items = find_items(&architecture);
    //Gets all the base-agents in the architecture.
    let agents = architecture.get_agents();
    //Iterate through the agents, and expand each of them in turn.
    for (_, agent) in architecture.get_agents() {
        let (i_agent, mut o_agent) = expand_agent(&agent, &items);
        //Adds the mfunc to the output agent created in this iteration.
        o_agent =
            agent_add_mfunc(&items, o_agent, &agents, &negative_constraints);
        architecture.insert_input_interface(i_agent);
        architecture.insert_output_interface(o_agent);
    }
    validate_architecture(positive_constraints, architecture)
        .expect("Positive constraints invalid!")
}

///Expands an agent, constructing two interface agents which are also given
///p and pi functions.
fn expand_agent(agent: &Agent, items: &Vec<Type>) -> (Agent, Agent) {
    let o_agent =
        agent_add_pfunc(Agent::empty(agent.clone_id()), &agent, &items);
    let i_agent =
        agent_add_pifunc(Agent::empty(agent.clone_id()), &agent, &items);
    (i_agent, o_agent)
}

///Adds the pifunc (which is responsible for all atomic constructions for
///interfaces) to an input agent.
fn agent_add_pifunc(
    mut i_agent: Agent,
    agent: &Agent,
    items: &Vec<Type>,
) -> Agent {
    for item in items {
        i_agent.add_constructor((
            item.clone(),
            vec![Type::certificate(agent.id(), &item.get_type_string())],
        ));
    }
    i_agent
}

///Adds the pfunc (which is responsible for all proof constructions)
///to an ouput agent.
fn agent_add_pfunc(
    mut o_agent: Agent,
    agent: &Agent,
    items: &Vec<Type>,
) -> Agent {
    for item in items {
        o_agent.add_constructor((
            Type::proof(agent.id(), &item.get_type_string()),
            vec![item.clone()],
        ));
    }
    o_agent
}

///Adds the mfunc (which is responsible for all certification constructions)
///to an output agent.
fn agent_add_mfunc(
    items: &Vec<Type>,
    mut o_agent: Agent,
    agents: &HashMap<Identity, Agent>,
    negative_constraints: &Vec<NegativeConstraint>,
) -> Agent {
    for item in items {
        for (id, _agent) in agents {
            let mut certification_construction_items: Vec<Type> = Vec::new();
            certification_construction_items.push(item.clone());
            for (t_agent, t_item, f_agent, f_item) in negative_constraints {
                if item == t_item && id == t_agent {
                    certification_construction_items
                        .push(Type::proof(&f_agent, &f_item.get_type_string()));
                }
            }
            o_agent.add_constructor((
                Type::certificate(&id, &item.get_type_string()),
                certification_construction_items,
            ));
        }
    }
    o_agent
}

///Iterates through all agents and finds all items which can be
///constructed within the architecture.
fn find_items(architecture: &Architecture) -> Vec<Type> {
    let mut items: Vec<Type> = Vec::new();
    for (_, agent) in architecture.get_agents() {
        for (_type, _) in agent.get_constructors() {
            items.push(_type);
        }
    }
    items
}

#[cfg(test)]

mod tests {
    use super::*;
    use crate::abstract_syntax::{
        abstract_syntax,
        eval,
    };

    #[test]
    fn test_basic_architecture() {
        let mut asppc: abstract_syntax::ASPPC = abstract_syntax::ASPPC::new();

        let mut agent1 = Agent::empty("Agent1".to_string());
        let mut agent2 = Agent::empty("Agent2".to_string());
        let mut i_agent1 = Agent::empty("Agent1".to_string());
        let mut o_agent1 = Agent::empty("Agent1".to_string());
        let mut i_agent2 = Agent::empty("Agent2".to_string());
        let mut o_agent2 = Agent::empty("Agent2".to_string());

        let item1 = Type::atomic(&"Item1".to_string());
        let ca1item1 =
            Type::certificate(&agent1.id(), &item1.get_type_string());
        let pa1item1 = Type::proof(&agent1.id(), &item1.get_type_string());
        let ca2item1 =
            Type::certificate(&agent2.id(), &item1.get_type_string());
        let pa2item1 = Type::proof(&agent2.id(), &item1.get_type_string());

        let item2 = Type::atomic(&"Item2".to_string());
        let ca1item2 =
            Type::certificate(&agent1.id(), &item2.get_type_string());
        let pa1item2 = Type::proof(&agent1.id(), &item2.get_type_string());
        let ca2item2 =
            Type::certificate(&agent2.id(), &item2.get_type_string());
        let pa2item2 = Type::proof(&agent2.id(), &item2.get_type_string());

        agent1.add_constructor((item1.clone(), vec![]));
        agent2.add_constructor((item2.clone(), vec![]));

        i_agent1.add_constructor((item2.clone(), vec![ca1item2.clone()]));
        i_agent1.add_constructor((item1.clone(), vec![ca1item1.clone()]));
        o_agent1.add_constructor((pa1item1.clone(), vec![item1.clone()]));
        o_agent1.add_constructor((pa1item2.clone(), vec![item2.clone()]));
        o_agent1.add_constructor((ca1item1.clone(), vec![item1.clone()]));
        o_agent1.add_constructor((
            ca1item2.clone(),
            vec![item2.clone(), pa2item1.clone()],
        ));
        o_agent1.add_constructor((ca2item1.clone(), vec![item1.clone()]));
        o_agent1.add_constructor((ca2item2.clone(), vec![item2.clone()]));

        i_agent2.add_constructor((item1.clone(), vec![ca2item1.clone()]));
        i_agent2.add_constructor((item2.clone(), vec![ca2item2.clone()]));
        o_agent2.add_constructor((pa2item2.clone(), vec![item2.clone()]));
        o_agent2.add_constructor((pa2item1.clone(), vec![item1.clone()]));
        o_agent2.add_constructor((ca1item1.clone(), vec![item1.clone()]));
        o_agent2.add_constructor((
            ca1item2.clone(),
            vec![item2.clone(), pa2item1.clone()],
        ));
        o_agent2.add_constructor((ca2item2.clone(), vec![item2.clone()]));
        o_agent2.add_constructor((ca2item1.clone(), vec![item1.clone()]));

        let mut manual_ex_architecture = Architecture::new();
        manual_ex_architecture.insert_agent(agent1.clone());
        manual_ex_architecture.insert_agent(agent2.clone());
        manual_ex_architecture.insert_input_interface(i_agent1.clone());
        manual_ex_architecture.insert_output_interface(o_agent1.clone());
        manual_ex_architecture.insert_input_interface(i_agent2.clone());
        manual_ex_architecture.insert_output_interface(o_agent2.clone());

        asppc.add_agent(agent1.clone_id());
        asppc.add_agent(agent2.clone_id());
        asppc.add_construction((agent1.clone_id(), item1.clone(), vec![]));
        asppc.add_construction((agent2.clone_id(), item2.clone(), vec![]));
        asppc.add_item(item1.clone());
        asppc.add_item(item2.clone());
        asppc.add_negative_constraint((
            agent1.clone_id(),
            item2.clone(),
            agent2.clone_id(),
            item1.clone(),
        ));

        let (architecture, neg, pos) = eval::eval_as(asppc);
        let ex_architecture = expand_architecture(architecture, neg, pos);
        assert_eq!(ex_architecture, manual_ex_architecture);
        //Can be used to get more exact errors.
        /*
        let a1 = ex_architecture.get_agents();
        let a2 = manual_ex_architecture.get_agents();
        for (afir, ifir) in &a1 {
            for (asec, isec) in &a2 {
                if (afir == asec) {
                    assert_eq!(ifir, isec);
                }
            }
        }
        */
    }

    #[test]
    fn test_architecture_three_agents() {
        let mut asppc: abstract_syntax::ASPPC = abstract_syntax::ASPPC::new();

        let mut agent1 = Agent::empty("Agent1".to_string());
        let mut agent2 = Agent::empty("Agent2".to_string());
        let agent3 = Agent::empty("Agent3".to_string());
        let mut i_agent1 = Agent::empty("Agent1".to_string());
        let mut o_agent1 = Agent::empty("Agent1".to_string());
        let mut i_agent2 = Agent::empty("Agent2".to_string());
        let mut o_agent2 = Agent::empty("Agent2".to_string());
        let mut i_agent3 = Agent::empty("Agent3".to_string());
        let mut o_agent3 = Agent::empty("Agent3".to_string());

        let item1 = Type::atomic(&"Item1".to_string());
        let ca1item1 =
            Type::certificate(&agent1.id(), &item1.get_type_string());
        let pa1item1 = Type::proof(&agent1.id(), &item1.get_type_string());
        let ca2item1 =
            Type::certificate(&agent2.id(), &item1.get_type_string());
        let pa2item1 = Type::proof(&agent2.id(), &item1.get_type_string());
        let ca3item1 =
            Type::certificate(&agent3.id(), &item1.get_type_string());
        let pa3item1 = Type::proof(&agent3.id(), &item1.get_type_string());

        let item2 = Type::atomic(&"Item2".to_string());
        let ca1item2 =
            Type::certificate(&agent1.id(), &item2.get_type_string());
        let pa1item2 = Type::proof(&agent1.id(), &item2.get_type_string());
        let ca2item2 =
            Type::certificate(&agent2.id(), &item2.get_type_string());
        let pa2item2 = Type::proof(&agent2.id(), &item2.get_type_string());
        let ca3item2 =
            Type::certificate(&agent3.id(), &item2.get_type_string());
        let pa3item2 = Type::proof(&agent3.id(), &item2.get_type_string());

        agent1.add_constructor((item1.clone(), vec![]));
        agent2.add_constructor((item2.clone(), vec![]));

        i_agent1.add_constructor((item2.clone(), vec![ca1item2.clone()]));
        i_agent1.add_constructor((item1.clone(), vec![ca1item1.clone()]));
        o_agent1.add_constructor((pa1item1.clone(), vec![item1.clone()]));
        o_agent1.add_constructor((pa1item2.clone(), vec![item2.clone()]));
        o_agent1.add_constructor((ca1item1.clone(), vec![item1.clone()]));
        o_agent1.add_constructor((
            ca1item2.clone(),
            vec![item2.clone(), pa3item2.clone()],
        ));
        o_agent1.add_constructor((ca2item1.clone(), vec![item1.clone()]));
        o_agent1.add_constructor((ca2item2.clone(), vec![item2.clone()]));
        o_agent1.add_constructor((ca3item1.clone(), vec![item1.clone()]));
        o_agent1.add_constructor((
            ca3item2.clone(),
            vec![item2.clone(), pa2item1.clone()],
        ));

        i_agent2.add_constructor((item1.clone(), vec![ca2item1.clone()]));
        i_agent2.add_constructor((item2.clone(), vec![ca2item2.clone()]));
        o_agent2.add_constructor((pa2item2.clone(), vec![item2.clone()]));
        o_agent2.add_constructor((pa2item1.clone(), vec![item1.clone()]));
        o_agent2.add_constructor((ca1item1.clone(), vec![item1.clone()]));
        o_agent2.add_constructor((
            ca1item2.clone(),
            vec![item2.clone(), pa3item2.clone()],
        ));
        o_agent2.add_constructor((ca2item2.clone(), vec![item2.clone()]));
        o_agent2.add_constructor((ca2item1.clone(), vec![item1.clone()]));
        o_agent2.add_constructor((
            ca3item2.clone(),
            vec![item2.clone(), pa2item1.clone()],
        ));
        o_agent2.add_constructor((ca3item1.clone(), vec![item1.clone()]));

        i_agent3.add_constructor((item1.clone(), vec![ca3item1.clone()]));
        i_agent3.add_constructor((item2.clone(), vec![ca3item2.clone()]));
        o_agent3.add_constructor((pa3item2.clone(), vec![item2.clone()]));
        o_agent3.add_constructor((pa3item1.clone(), vec![item1.clone()]));
        o_agent3.add_constructor((ca1item1.clone(), vec![item1.clone()]));
        o_agent3.add_constructor((
            ca1item2.clone(),
            vec![item2.clone(), pa3item2.clone()],
        ));
        o_agent3.add_constructor((ca2item2.clone(), vec![item2.clone()]));
        o_agent3.add_constructor((ca2item1.clone(), vec![item1.clone()]));
        o_agent3.add_constructor((
            ca3item2.clone(),
            vec![item2.clone(), pa2item1.clone()],
        ));
        o_agent3.add_constructor((ca3item1.clone(), vec![item1.clone()]));

        let mut manual_ex_architecture = Architecture::new();
        manual_ex_architecture.insert_agent(agent1.clone());
        manual_ex_architecture.insert_agent(agent2.clone());
        manual_ex_architecture.insert_agent(agent3.clone());
        manual_ex_architecture.insert_input_interface(i_agent1.clone());
        manual_ex_architecture.insert_output_interface(o_agent1.clone());
        manual_ex_architecture.insert_input_interface(i_agent2.clone());
        manual_ex_architecture.insert_output_interface(o_agent2.clone());
        manual_ex_architecture.insert_input_interface(i_agent3.clone());
        manual_ex_architecture.insert_output_interface(o_agent3.clone());

        asppc.add_agent(agent1.clone_id());
        asppc.add_agent(agent2.clone_id());
        asppc.add_agent(agent3.clone_id());
        asppc.add_construction((agent1.clone_id(), item1.clone(), vec![]));
        asppc.add_construction((agent2.clone_id(), item2.clone(), vec![]));
        asppc.add_item(item1.clone());
        asppc.add_item(item2.clone());
        asppc.add_negative_constraint((
            agent1.clone_id(),
            item2.clone(),
            agent3.clone_id(),
            item2.clone(),
        ));
        asppc.add_negative_constraint((
            agent3.clone_id(),
            item2.clone(),
            agent2.clone_id(),
            item1.clone(),
        ));

        let (architecture, neg, pos) = eval::eval_as(asppc);
        let ex_architecture = expand_architecture(architecture, neg, pos);
        assert_eq!(ex_architecture, manual_ex_architecture);
        //Can be used to get more exact errors.
        /*
        let a1 = ex_architecture.get_agents();
        let a2 = manual_ex_architecture.get_agents();
        for (afir, ifir) in &a1 {
            for (asec, isec) in &a2 {
                if (afir == asec) {
                    assert_eq!(ifir, isec);
                }
            }
        }
        */
    }
}
