#[macro_use]
extern crate derive_builder;

pub mod abstract_syntax;
#[macro_use]
pub mod agent;
pub mod architecture;
pub mod expand;
pub mod identity;
pub mod keycollection;
pub mod message;
pub mod participant;
pub mod possession_handler;
mod test;
pub mod validate;
mod validator;
pub use agent::Agent;
use agent::AgentError;
pub use architecture::Architecture;
use architecture::ArchitectureError;
pub use identity::Identity;
pub use message::Certificate;
pub use message::Message;
pub use message::Proof;
pub use message::Term;
pub use message::Type;
pub use participant::participant::Participant;
pub use possession_handler::PossessionHandler;
mod keyring;
pub use keyring::{
    KeyQuartette,
    Keyring,
};
pub use validator::Validator;

#[macro_use]
extern crate guard;

/// Enum for returning more descriptive errors than Err(())
#[derive(Debug)]
pub enum Error {
    Architecture(ArchitectureError),
    Agent(AgentError),
}
