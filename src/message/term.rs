use serde::{
    Deserialize,
    Serialize,
};

use super::{
    Certificate,
    MessageError,
    Proof,
};
use crate::{
    Identity,
    Keyring,
    Type,
};

/// A typed container of data that agents can share between each other by
/// sending `Message`:s.
#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub struct Term<T> {
    _type: Type,
    body: T,
}

impl<T> Term<T> {
    fn new(_type: Type, body: T) -> Term<T> { Term { _type, body } }

    /// Creates an atomic term.
    pub fn atomic(atomic: String, body: T) -> Term<T> {
        Term {
            _type: Type::Atomic(atomic),
            body,
        }
    }

    /// Getter for `_type`.
    pub fn _type(&self) -> &Type { &self._type }

    /// Getter for `body`.
    pub fn body(&self) -> &T { &self.body }

    /// Applies a lambda on the body of the term.
    pub fn map<F, U>(self, lambda: F) -> Term<U>
    where
        F: Fn(T) -> U,
    {
        Term::new(self._type, lambda(self.body))
    }

    /// Applies a lambda on the body of the term.
    pub fn map_mut<F, U>(self, lambda: F) -> Term<U>
    where
        F: FnMut(T) -> U,
    {
        let mut lambda = lambda;
        Term::new(self._type, lambda(self.body))
    }

    /// Like `map` but will propagate errors thrown in the lambda.
    pub fn try_map<F, U, E>(self, lambda: F) -> Result<Term<U>, E>
    where
        F: Fn(T) -> Result<U, E>,
    {
        let body = lambda(self.body)?;
        Ok(Term::new(self._type, body))
    }

    /// Clears the body of the term.
    pub fn clear(self) -> Term<()> { self.map(|_| ()) }
}

// Proof
// -----------------------------------------------------------------------------

impl<T> Term<T> {
    /// Create a proof that the agent with the given id has been in possession
    /// of the term.
    pub fn prove(
        self,
        keyring: &impl Keyring,
        id: Identity,
    ) -> Result<Term<Proof>, MessageError> {
        let body = Proof::new(keyring, &id, &self._type)?;
        let _type = self._type.prove(id.clone());
        Ok(Term::new(_type, body))
    }
}

impl<T> Term<T>
where
    T: Into<Proof>,
{
    /// Verifies the term.
    pub fn verify(self, keyring: &impl Keyring) -> Result<(), MessageError> {
        let signature = self.body.into();
        match &self._type {
            Type::Proof(id, _type) => signature.verify(keyring, id, _type),
            _ => Err(MessageError::InvalidType(self._type)),
        }
    }
}

// Certificate
// -----------------------------------------------------------------------------

impl<T> Term<T>
where
    T: Into<Vec<u8>>,
{
    /// Certifies the term so that only the agent with the given id can decrypt
    /// its body.
    pub fn certify(
        self,
        keyring: &impl Keyring,
        id: Identity,
    ) -> Result<Term<Certificate>, MessageError> {
        let key = keyring
            .public_encryption_key(&id)
            .map_err(|error| MessageError::KeyringError(error.into()))?;
        let body = Certificate::certify(&key, self.body.into().as_slice())?;
        Ok(Term {
            _type: self._type.certify(id),
            body,
        })
    }
}

impl<T> Term<T>
where
    T: Into<Certificate>,
{
    /// Decertify the term.
    pub fn decertify(
        self,
        keyring: &impl Keyring,
    ) -> Result<Term<Vec<u8>>, MessageError> {
        use MessageError::*;

        match self._type {
            Type::Certificate(id, _type) => {
                let key =
                    keyring.secret_encryption_key(&id).map_err(|error| {
                        MessageError::KeyringError(error.into())
                    })?;
                let body = self.body.into().decertify(&key)?;
                Ok(Term::new(*_type, body))
            }
            _ => Err(InvalidType(self._type)),
        }
    }
}

// -----------------------------------------------------------------------------
// Tests
// -----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use rand::{
        Rng,
        RngCore,
    };

    use super::*;
    use crate::test::{
        arbitrary,
        MockKeyring,
    };

    #[test]
    fn map() {
        // Generate some test data.
        let mut rng = rand::thread_rng();
        let n = rng.gen_range(16, 32) as usize;
        let mut data = Vec::<Vec<u8>>::new();
        for _ in 0..n {
            let m = rng.gen_range(16, 32);
            let mut buffer = vec![0; m];
            rng.fill_bytes(buffer.as_mut_slice());
            data.push(buffer);
        }

        // Create a term.
        let k = rng.next_u64() as usize;
        let term = Term::atomic(String::from("BILBO"), k);

        // Store expected results.
        let _type = term._type().clone();
        let body = &data[k % n];

        // Map message.
        let term = term.map(|k| &data[k % n]);

        // Check if results are as expected.
        assert_eq!(term._type(), &_type);
        assert_eq!(term.body(), &body);
    }

    // Proof
    // -------------------------------------------------------------------------

    #[test]
    fn prove_and_validate() {
        let mut rng = rand::thread_rng();

        let mut keyring = MockKeyring::builder()
            .size(rng.gen_range(4, 8))
            .secret_id_probability(1.0)
            .gen(&mut rng);

        let id = keyring.arbitrary_secret_id().clone();

        let term = arbitrary::atomic_term();
        let term = term.prove(&keyring, id.clone()).unwrap();

        // Remove the secret keys for the sign id since validation should work
        // with only the public sign key.
        keyring.remove_secret_keys(&id);
        term.verify(&keyring).unwrap();

        // Create a term with a more complex type.
        let term = arbitrary::term(&keyring);

        // Prove the term.
        let id = keyring.arbitrary_secret_id().clone();
        let term = term.prove(&keyring, id.clone()).unwrap();

        // Remove the secret keys and try to verify.
        keyring.remove_secret_keys(&id);
        term.verify(&keyring).unwrap();
    }

    #[test]
    fn prove_without_secret_keys() {
        let mut rng = rand::thread_rng();

        let keyring = MockKeyring::builder()
            .size(rng.gen_range(4, 8))
            .secret_id_probability(0.0)
            .gen(&mut rng);

        let id = keyring.arbitrary_public_id();
        let term = arbitrary::atomic_term();
        assert!(matches!(
            term.prove(&keyring, id.clone()),
            Err(MessageError::KeyringError(_))
        ));
    }

    // Certificate.
    // -------------------------------------------------------------------------

    #[test]
    fn certify_and_decertify() {
        let mut rng = rand::thread_rng();

        let keyring = MockKeyring::builder()
            .exclude_public_keys(false)
            .size(rng.gen_range(4, 8))
            .secret_id_probability(1.0)
            .gen(&mut rng);

        let mut term = arbitrary::atomic_term();

        // Store the expected result.
        let plain = term.body().clone();

        for i in 1..5 {
            // Certify and then decertify the message `i` times.
            for _ in 0..i {
                let id = keyring.arbitrary_secret_id();
                term =
                    term.certify(&keyring, id.clone()).unwrap().map(Vec::from);
            }
            for _ in 0..i {
                term = term.decertify(&keyring).unwrap();
            }

            assert_eq!(term.body(), &plain);
        }
    }

    #[test]
    fn decertify_without_secret_key() {
        let mut rng = rand::thread_rng();

        let keyring = MockKeyring::builder()
            .size(rng.gen_range(4, 8))
            .secret_id_probability(0.0)
            .gen(&mut rng);

        let mut message = arbitrary::atomic_term();

        let id = keyring.arbitrary_public_id().clone();
        message = message.certify(&keyring, id).unwrap().map(Vec::from);

        message.decertify(&keyring).expect_err(
            "should not be possible to decertify message without secret key",
        );
    }

    #[test]
    fn decertify_non_certificate() {
        let mut rng = rand::thread_rng();

        let keyring = MockKeyring::builder()
            .size(1)
            .min_secret_ids(1)
            .gen(&mut rng);

        let atomic = arbitrary::atomic_term();
        let proof = atomic
            .clone()
            .prove(&keyring, keyring.arbitrary_secret_id().clone())
            .unwrap()
            .map(Vec::from);

        atomic
            .decertify(&keyring)
            .expect_err("should not be possible to decertify atomic");
        proof
            .decertify(&keyring)
            .expect_err("should not be possible to decertify proof");
    }
}
