use std::convert::TryInto;

use ed25519_dalek::{
    Signature,
    Signer,
    Verifier,
};
use serde::{
    Deserialize,
    Serialize,
};

use super::MessageError;
use crate::{
    Identity,
    Keyring,
    Type,
};

/// Sign data with the key associated with the agent with the id.
pub fn sign(
    keyring: &impl Keyring,
    signer: &Identity,
    data: &[u8],
) -> Result<Vec<u8>, MessageError> {
    let key = keyring
        .secret_sign_key(signer)
        .map_err(|error| MessageError::KeyringError(error.into()))?;
    Ok(key.sign(data).to_bytes().to_vec())
}

/// Verifies that the signer created the signature for the given data.
pub fn verify<'a>(
    keyring: &impl Keyring,
    signer: &'a Identity,
    signature: &[u8],
    data: &[u8],
) -> Result<(), MessageError> {
    use MessageError::*;

    let key = keyring
        .public_sign_key(signer)
        .map_err(|error| KeyringError(error.into()))?;
    let signature =
        Signature::new(signature.try_into().or(Err(InvalidSignature))?);
    key.verify(data, &signature).or(Err(FailedVerification))
}

/// A proof is a signature that proves that a certain agent has been in
/// possession of a certain type.
#[derive(Clone, Serialize, Deserialize, PartialEq, Debug)]
pub struct Proof(Vec<u8>);

impl Proof {
    /// Creates a new proof.
    pub fn new(
        keyring: &impl Keyring,
        id: &Identity,
        _type: &Type,
    ) -> Result<Proof, MessageError> {
        let plain = serde_json::to_string(_type).unwrap();
        let signature = sign(keyring, id, plain.as_bytes())?;
        Ok(Proof(signature))
    }

    /// Checks the authenticity of a proof and returns the result.
    pub fn verify(
        &self,
        keyring: &impl Keyring,
        id: &Identity,
        _type: &Type,
    ) -> Result<(), MessageError> {
        let data = serde_json::to_string(_type).unwrap();
        verify(keyring, id, self.0.as_slice(), data.as_bytes())
    }
}

// -------------------------------------------------------------------------------------------------
// Byte data conversions.
// -------------------------------------------------------------------------------------------------

impl Proof {
    /// Returns a slice of the proof.
    pub fn as_slice(&self) -> &[u8] { self.0.as_slice() }
}

impl From<Vec<u8>> for Proof {
    fn from(data: Vec<u8>) -> Self { Proof(data) }
}

impl From<Proof> for Vec<u8> {
    fn from(proof: Proof) -> Self { proof.0 }
}
