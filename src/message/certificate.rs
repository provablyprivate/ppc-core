use std::convert::TryInto;

use crypto_box::{
    aead::Aead,
    PublicKey,
    SecretKey,
};
use serde::{
    Deserialize,
    Serialize,
    Serializer,
};
use xsalsa20poly1305::{
    Nonce,
    NONCE_SIZE,
};

use super::MessageError;

fn shared_secret_authentication_key() -> SecretKey { SecretKey::from([7; 32]) }

fn shared_public_authentication_key() -> PublicKey {
    shared_secret_authentication_key().public_key()
}

#[derive(Deserialize, Clone, Debug)]
pub struct Certificate {
    nonce: [u8; NONCE_SIZE],
    cipher: Vec<u8>,
}

impl From<&Certificate> for Vec<u8> {
    fn from(certificate: &Certificate) -> Vec<u8> {
        let mut vector = certificate.nonce.to_vec();
        vector.extend(certificate.cipher.clone());
        vector
    }
}

impl From<Certificate> for Vec<u8> {
    fn from(certificate: Certificate) -> Vec<u8> {
        let mut vector = certificate.nonce.to_vec();
        vector.extend(certificate.cipher);
        vector
    }
}

impl From<Vec<u8>> for Certificate {
    fn from(vector: Vec<u8>) -> Certificate {
        let nonce: [u8; NONCE_SIZE] = vector[0..NONCE_SIZE].try_into().unwrap();
        let cipher = vector[NONCE_SIZE..].to_vec();
        Certificate { nonce, cipher }
    }
}

impl Serialize for Certificate {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let data: Vec<u8> = Vec::from(self);
        data.serialize(serializer)
    }
}

impl Certificate {
    /// Creates a `Certificate` from the data for the agent associated with
    /// `id`.
    pub fn certify(
        key: &PublicKey,
        data: &[u8],
    ) -> Result<Certificate, MessageError> {
        use MessageError::*;

        let secret_key = shared_secret_authentication_key();
        let _box = crypto_box::Box::new(key, &secret_key);

        let mut rng = rand::thread_rng();
        let nonce = crypto_box::generate_nonce(&mut rng);
        let cipher = _box.encrypt(&nonce, data).or(Err(FailedEncryption))?;

        Ok(Certificate {
            nonce: nonce.as_slice().try_into().unwrap(),
            cipher,
        })
    }

    /// Decertifies the `Certificate`.
    pub fn decertify(self, key: &SecretKey) -> Result<Vec<u8>, MessageError> {
        use MessageError::*;

        let public_key = shared_public_authentication_key();
        let _box = crypto_box::Box::new(&public_key, key);

        let nonce = Nonce::from(self.nonce);
        let cipher = self.cipher.as_slice();

        _box.decrypt(&nonce, cipher).or(Err(FailedDecryption))
    }
}
