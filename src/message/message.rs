use serde::{
    Deserialize,
    Serialize,
};

use super::{
    proof::{
        sign,
        verify,
    },
    MessageError,
};
use crate::{
    Identity,
    Keyring,
    Term,
};

pub type Trace = Vec<Message<()>>;

/// The structure used to share data within a ppc system.
///
/// In ppc-core, terms shared between agents should be wrapped by `Message<T>`.
/// This way any participant being in possession of the architecture and keys
/// can validate with a single message that all previous communication has been
/// done correctly.
#[derive(Debug, Serialize, Deserialize, Clone, PartialEq, Builder)]
#[builder(pattern = "owned")]
pub struct Message<T> {
    sender: Identity,
    receiver: Identity,
    term: Term<T>,

    #[builder(default = "Trace::new()")]
    trace: Trace,

    #[builder(setter(skip))]
    signature: Vec<u8>,
}

impl<T> Message<T> {
    /// A builder for creating `Message<T>`. Required fields to set is `sender`,
    /// `receiver` and `term`. The field `trace` can be set optionaly. If not
    /// set it defaults to an empty trace.
    ///
    /// To finalize the builder into a message call `sign(&keyring)` where
    /// `keyring` implements the trait `Keyring`.
    ///
    /// ## Example
    /// ```
    /// use std::collections::HashMap;
    ///
    /// use ppc::{
    ///     Identity,
    ///     KeyQuartette,
    ///     Message,
    ///     Term,
    /// };
    ///
    /// let mut rng = rand::thread_rng();
    /// let keyring: HashMap<_, _> = vec![
    ///     (Identity::from("Hamlet"), KeyQuartette::gen(&mut rng)),
    ///     (Identity::from("Skull"), KeyQuartette::gen(&mut rng)),
    /// ]
    /// .into_iter()
    /// .collect();
    ///
    /// let message = Message::builder()
    ///     .sender(Identity::from("Hamlet"))
    ///     .receiver(Identity::from("Skull"))
    ///     .term(Term::atomic(String::from("QUOTE"), "to be or not to be"))
    ///     .sign(&keyring)
    ///     .unwrap();
    /// ```
    pub fn builder() -> MessageBuilder<T> { MessageBuilder::default() }

    /// Creates a new message.
    pub fn new(
        keyring: &impl Keyring,
        sender: Identity,
        receiver: Identity,
        term: Term<T>,
    ) -> Result<Message<T>, MessageError> {
        Message::builder()
            .sender(sender)
            .receiver(receiver)
            .term(term)
            .trace(Trace::new())
            .sign(keyring)
    }

    pub fn map<F, U>(self, lambda: F) -> Message<U>
    where
        F: Fn(T) -> U,
    {
        Message {
            sender: self.sender,
            receiver: self.receiver,
            signature: self.signature,
            term: self.term.map(lambda),
            trace: self.trace,
        }
    }

    pub fn map_mut<F, U>(self, lambda: F) -> Message<U>
    where
        F: FnMut(T) -> U,
    {
        Message {
            sender: self.sender,
            receiver: self.receiver,
            signature: self.signature,
            term: self.term.map_mut(lambda),
            trace: self.trace,
        }
    }

    // Validation
    // -------------------------------------------------------------------------

    pub fn verify_signature(
        &self,
        keyring: &impl Keyring,
    ) -> Result<(), MessageError> {
        let data = (&self.receiver, self.term._type());
        let data = serde_json::to_string(&data).unwrap();
        verify(keyring, &self.sender, &self.signature, &data.as_bytes())
    }

    fn valid(&self, keyring: &impl Keyring) -> Result<(), MessageError> {
        self.verify_signature(keyring)?;

        self.trace.iter().try_fold((), |_, message| {
            if &message.receiver == &self.sender {
                message.valid(keyring)
            } else {
                Err(MessageError::InvalidPossessionClaim)
            }
        })
    }

    /// Transforms a reference to the messages into a [`Result<Message<T>,
    /// MessageError>`], mapping valid messages to [`Ok(&message)`] and
    /// invalid messages to [`Err(MessageError)`].
    ///
    /// ## Example
    /// Messages are validated when info about them are read. To trigger a
    /// validation without reading data one can check if a messages is valid by
    /// using `as_ok`.
    /// ```
    /// use std::collections::HashMap;
    ///
    /// use ppc::{
    ///     Identity,
    ///     KeyQuartette,
    ///     Keyring,
    ///     Message,
    ///     Term,
    /// };
    ///
    /// // Create a keyring.
    /// let mut rng = rand::thread_rng();
    /// let keyring: HashMap<_, _> = vec![
    ///     (Identity::from("Alpha"), KeyQuartette::gen(&mut rng)),
    ///     (Identity::from("Beta"), KeyQuartette::gen(&mut rng)),
    /// ]
    /// .into_iter()
    /// .collect();
    ///
    /// // Create a message.
    /// let sender = Identity::from("Alpha");
    /// let receiver = Identity::from("Beta");
    /// let term =
    ///     Term::atomic(String::from("INFO"), "I love hamsters and privacy!");
    /// let message = Message::new(&keyring, sender, receiver, term).unwrap();
    ///
    /// assert!(message.as_ok(&keyring).is_ok());
    /// ```
    pub fn as_ok(
        &self,
        keyring: &impl Keyring,
    ) -> Result<&Message<T>, MessageError> {
        self.valid(keyring).map(|_| self)
    }

    /// Transforms the message into a [`Result<Message<T>, MessageError>`],
    /// mapping valid messages to [`Ok(message)`] and invalid messages to
    /// [`Err(MessageError)`].
    pub fn to_ok(
        self,
        keyring: &impl Keyring,
    ) -> Result<Message<T>, MessageError> {
        self.valid(keyring).map(|_| self)
    }

    // Getters
    // -------------------------------------------------------------------------

    /// Gets the sender of the message. Fails if message is not valid.
    pub fn sender(
        &self,
        keyring: &impl Keyring,
    ) -> Result<&Identity, MessageError> {
        self.valid(keyring).map(|_| &self.sender)
    }

    /// Gets the receiver of the message. Fails if message is not valid.
    pub fn receiver(
        &self,
        keyring: &impl Keyring,
    ) -> Result<&Identity, MessageError> {
        self.valid(keyring).map(|_| &self.receiver)
    }

    /// Gets the term of the message. Fails if message is not valid.
    pub fn term(
        &self,
        keyring: &impl Keyring,
    ) -> Result<&Term<T>, MessageError> {
        self.valid(keyring).map(|_| &self.term)
    }

    /// Consumes the message and returns the `Term<T>` inside of it. Fails if
    /// message is not valid.
    pub fn into_term(
        self,
        keyring: &impl Keyring,
    ) -> Result<Term<T>, MessageError> {
        self.valid(keyring).map(|_| self.term)
    }

    /// Gets the term of the message. Fails if message is not valid.
    pub fn trace(
        &self,
        keyring: &impl Keyring,
    ) -> Result<&Trace, MessageError> {
        self.valid(keyring).map(|_| &self.trace)
    }

    // Trace operations.
    // -------------------------------------------------------------------------

    /// Attatches a possession to the trace of the message. Fails if the
    /// possession is an invalid attachment.
    pub fn attach(
        &mut self,
        keyring: &impl Keyring,
        message: Message<()>,
    ) -> Result<(), MessageError> {
        if self.sender == message.receiver {
            message
                .to_ok(keyring)
                .map(|message| self.trace.push(message))
        } else {
            Err(MessageError::InvalidPossessionAttached)
        }
    }

    /// Transforms the `Message<T>` into a `Message<()>`. This makes it possible
    /// to attach the message to other messages trace.
    pub fn into_attachment(self) -> Message<()> { self.map(|_| ()) }
}

// -----------------------------------------------------------------------------
// Builder
// -----------------------------------------------------------------------------

impl<T> MessageBuilder<T> {
    pub fn sign(
        self,
        keyring: &impl Keyring,
    ) -> Result<Message<T>, MessageError> {
        let message = self.build()?;
        let data = (&message.receiver, message.term._type());
        let data = serde_json::to_string(&data).unwrap();
        let signature = sign(keyring, &message.sender, data.as_bytes())?;
        Ok(Message {
            sender: message.sender,
            receiver: message.receiver,
            term: message.term,
            trace: message.trace,
            signature,
        })
    }
}

// -----------------------------------------------------------------------------
// Tests
// -----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use rand::{
        CryptoRng,
        Rng,
        RngCore,
    };

    use super::*;
    use crate::test::{
        arbitrary,
        MockKeyring,
    };

    fn gen_different_u8<R>(rng: &mut R, value: u8) -> u8
    where
        R: RngCore,
    {
        loop {
            let other = rng.next_u32() as u8;
            if value != other {
                return other;
            }
        }
    }

    fn generate_message<R>(
        rng: &mut R,
        keyring: &MockKeyring,
        receiver: Identity,
        error: Option<&MessageError>,
    ) -> Message<Vec<u8>>
    where
        R: RngCore + CryptoRng,
    {
        use MessageError::*;

        let mut message = arbitrary::message_with_receiver(keyring, receiver);
        attach_trace(keyring, &mut message, 4);

        if let Some(error) = error {
            match error {
                InvalidSignature => {
                    let mut buffer = Vec::new();

                    let size = if rng.gen() {
                        rng.gen_range(0, message.signature.len() - 1)
                    } else {
                        rng.gen_range(
                            message.signature.len() + 1,
                            message.signature.len() * 2,
                        )
                    };

                    buffer.resize(size, 0);
                    rng.fill_bytes(buffer.as_mut_slice());
                    message.signature = buffer;
                }
                FailedVerification => {
                    // Either modifies the signature or changes the type of the
                    // term.
                    if rng.gen() {
                        let index = rng.gen_range(0, message.signature.len());
                        let value =
                            gen_different_u8(rng, message.signature[index]);
                        message.signature[index] = value;
                    } else {
                        message.term = arbitrary::term(keyring);
                    }
                }
                InvalidPossessionClaim => {
                    let trace = &mut message.trace;
                    let index = rng.gen_range(0, trace.len());
                    let attachment = &mut trace[index];
                    loop {
                        let other = keyring.arbitrary_public_id();
                        if other != &attachment.receiver {
                            attachment.receiver = other.clone();
                            break;
                        }
                    }
                }
                _ => panic!(format!(
                    "can not generate message with error {:?}",
                    error
                )),
            }
        }
        message
    }

    fn attach_trace<T>(
        keyring: &MockKeyring,
        message: &mut Message<T>,
        max_depth: usize,
    ) {
        if max_depth < 1 {
            return;
        }

        let count = rand::thread_rng().gen_range(2, 4);

        message.trace = (0..count)
            .map(|_| {
                let mut attachment = arbitrary::message_with_receiver(
                    &keyring,
                    message.sender.clone(),
                );
                attach_trace(
                    &keyring,
                    &mut attachment,
                    rand::thread_rng().gen_range(0, max_depth),
                );
                attachment.into_attachment()
            })
            .collect()
    }

    #[test]
    fn valid_message() {
        let mut rng = rand::thread_rng();

        let keyring = MockKeyring::builder()
            .exclude_public_keys(false)
            .size(rng.gen_range(8, 16))
            .min_secret_ids(1)
            .min_public_ids(1)
            .secret_id_probability(rng.gen_range(0.0, 1.0))
            .gen(&mut rng);

        let mut message = arbitrary::message(&keyring);

        // Check that the generated message is valid.
        message.as_ok(&keyring).unwrap();

        // Attach trace to the message and check that it is still valid.
        attach_trace(&keyring, &mut message, 1);
        message.as_ok(&keyring).unwrap();

        // Add some depth to the trace and check that it is still valid.
        for attachment in message.trace.iter_mut() {
            attach_trace(&keyring, attachment, rng.gen_range(4, 8));
        }
        message.as_ok(&keyring).unwrap();
    }

    fn default_keyring<R>(rng: &mut R) -> MockKeyring
    where
        R: RngCore + CryptoRng,
    {
        MockKeyring::builder()
            .exclude_public_keys(false)
            .size(rng.gen_range(8, 16))
            .min_secret_ids(1)
            .min_public_ids(2)
            .secret_id_probability(rng.gen_range(0.0, 1.0))
            .gen(rng)
    }

    fn test_message(error: Option<MessageError>) {
        use MessageError::*;

        let mut rng = rand::thread_rng();
        let keyring = default_keyring(&mut rng);

        let message = generate_message(
            &mut rng,
            &keyring,
            keyring.arbitrary_public_id().clone(),
            error.as_ref(),
        )
        .to_ok(&keyring);

        if let Some(error) = error {
            match error {
                FailedVerification => {
                    assert!(matches!(message.unwrap_err(), FailedVerification))
                }
                InvalidSignature => {
                    assert!(matches!(message.unwrap_err(), InvalidSignature))
                }
                InvalidPossessionClaim => {
                    assert!(matches!(
                        message.unwrap_err(),
                        InvalidPossessionClaim
                    ))
                }
                _ => panic!("can not test message error {:?}", error),
            };
        }
    }

    #[test]
    fn invalid_signature() {
        test_message(Some(MessageError::InvalidSignature))
    }

    #[test]
    fn failed_verification() {
        test_message(Some(MessageError::FailedVerification))
    }

    #[test]
    fn invalid_possession_claim() {
        test_message(Some(MessageError::InvalidPossessionClaim))
    }

    #[test]
    fn invalid_possession_attached() {
        use MessageError::*;

        let mut rng = rand::thread_rng();
        let keyring = default_keyring(&mut rng);

        let mut message = generate_message(
            &mut rng,
            &keyring,
            keyring.arbitrary_public_id().clone(),
            None,
        );

        for error in
            [InvalidSignature, FailedVerification, InvalidPossessionClaim]
                .iter()
        {
            let attachment = generate_message(
                &mut rng,
                &keyring,
                message.receiver.clone(),
                Some(error),
            )
            .into_attachment();

            let len = message.trace.len();

            assert!(matches!(
                message.attach(&keyring, attachment).unwrap_err(),
                InvalidPossessionAttached
            ));
            assert_eq!(len, message.trace.len());
        }
    }

    #[test]
    fn builder() {
        let mut rng = rand::thread_rng();
        let keyring = MockKeyring::builder()
            .exclude_public_keys(false)
            .size(2)
            .min_secret_ids(1)
            .min_public_ids(1)
            .gen(&mut rng);

        let id_a = keyring.arbitrary_secret_id();
        let id_b = keyring.arbitrary_public_id();
        let term = Term::atomic(String::from("A"), ());

        let data = (&id_b, term._type());
        let data = serde_json::to_string(&data).unwrap();
        let signature = sign(&keyring, &id_a, data.as_bytes()).unwrap();

        let expect = Message {
            sender: id_a.clone(),
            receiver: id_b.clone(),
            term: term.clone(),
            trace: Vec::new(),
            signature,
        };
        expect.as_ok(&keyring).unwrap();

        let message = Message::builder()
            .sender(id_a.clone())
            .receiver(id_b.clone())
            .term(term)
            .sign(&keyring)
            .unwrap();
        message.as_ok(&keyring).unwrap();

        assert_eq!(message, expect);
    }
}
