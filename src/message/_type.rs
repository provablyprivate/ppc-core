use serde::{
    Deserialize,
    Serialize,
};

use crate::Identity;

/// A `Type` is a description of the type of content sent in a message between
/// two agents.
///
/// Together with the right keys it also contains the information needed to
/// encrypt and decrypt the content of the message.
#[derive(Clone, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub enum Type {
    Atomic(String),
    Certificate(Identity, Box<Type>),
    Proof(Identity, Box<Type>),
}

impl Type {
    /// Convenience function for creating `Type::Atomic`.
    pub fn atomic(atomic: &str) -> Type { Type::Atomic(String::from(atomic)) }

    /// Convenience function for creating `Type::Certificate`.
    pub fn certificate(id: &str, atomic: &str) -> Type {
        Type::Certificate(Identity::from(id), Box::new(Type::atomic(atomic)))
    }

    /// Convenience function for creating `Type::Proof`.
    pub fn proof(id: &str, atomic: &str) -> Type {
        Type::Proof(Identity::from(id), Box::new(Type::atomic(atomic)))
    }

    ///Gets the atomic type of a type, even if nestled.
    pub fn get_type_string(&self) -> String {
        match self {
            Type::Atomic(string) => string.to_string().clone(),
            Type::Certificate(_agent, item) => item.get_type_string(),
            Type::Proof(_agent, item) => item.get_type_string(),
        }
    }

    /// Pushes a certificate for a given identity of an agent on top of a type.
    ///
    /// ## Example
    /// ```
    /// use ppc::{
    ///     Identity,
    ///     Type,
    /// };
    ///
    /// let t1 = Type::atomic("A").certify(Identity::from("Alpha"));
    /// let t2 = Type::certificate("Alpha", "A");
    ///
    /// assert_eq!(t1, t2);
    /// ```
    pub fn certify(self, id: Identity) -> Type {
        Type::Certificate(id, Box::new(self))
    }

    /// Pushes a proof for a given identity of an agent on top of a type.
    pub fn prove(self, id: Identity) -> Type { Type::Proof(id, Box::new(self)) }

    /// Iterator that iterates through all certificates and proofs of the type
    /// down to the root atomic.
    pub fn iter(&self) -> TypeIterator { TypeIterator::new(self) }

    /// Returns tail element of _type or `None` if the type is atomic.
    ///
    /// ## Example
    /// ```
    /// use ppc::{
    ///     Identity,
    ///     Type,
    /// };
    ///
    /// let t0 = Type::proof("Alpha", "A");
    /// let t1 = Type::atomic("A");
    ///
    /// assert_eq!(t0.tail(), Some(&t1));
    /// ```
    pub fn tail(&self) -> Option<&Type> {
        match self {
            Type::Atomic(_) => None,
            Type::Certificate(_, tail) => Some(tail),
            Type::Proof(_, tail) => Some(tail),
        }
    }

    /// Returns true if the type is of atomic type.
    pub fn is_atomic(&self) -> bool { matches!(self, Type::Atomic(_)) }

    /// Returns true if the type is of certificate type.
    pub fn is_certificate(&self) -> bool {
        matches!(self, Type::Certificate(_, _))
    }

    /// Returns true if the type is of proof type.
    pub fn is_proof(&self) -> bool { matches!(self, Type::Proof(_, _)) }
}

/// The structure returned when calling the `iter` function of `Type`.
///
/// ## NOTE:
/// This type is never supposed to be initialized directly.
pub struct TypeIterator<'a>(Option<&'a Type>);

impl<'a> TypeIterator<'a> {
    fn new(_type: &Type) -> TypeIterator { TypeIterator(Some(_type)) }
}

impl<'a> Iterator for TypeIterator<'a> {
    type Item = &'a Type;

    fn next(&mut self) -> Option<Self::Item> {
        let head = self.0.take()?;
        let tail = head.tail();
        self.0 = tail;
        Some(head)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::Identity;

    #[test]
    fn constructors() {
        let expect = Type::Atomic(String::from("A"));
        let result = Type::atomic("A");
        assert_eq!(expect, result);

        let expect = Type::Certificate(
            Identity::from("Alpha"),
            Box::new(Type::Atomic(String::from("A"))),
        );
        let result = Type::certificate("Alpha", "A");
        assert_eq!(expect, result);

        let expect = Type::Proof(
            Identity::from("Alpha"),
            Box::new(Type::Atomic(String::from("A"))),
        );
        let result = Type::proof("Alpha", "A");
        assert_eq!(expect, result);
    }

    #[test]
    fn push_certificates_and_proofs() {
        let expect = Type::Atomic(String::from("A"));
        let result = Type::atomic("A");
        assert_eq!(expect, result);

        let expect =
            Type::Certificate(Identity::from("Alpha"), Box::new(expect));
        let result = result.certify(Identity::from("Alpha"));
        assert_eq!(expect, result);

        let expect = Type::Proof(Identity::from("Beta"), Box::new(expect));
        let result = result.prove(Identity::from("Beta"));
        assert_eq!(expect, result);

        let expect =
            Type::Certificate(Identity::from("Gamma"), Box::new(expect));
        let result = result.certify(Identity::from("Gamma"));
        assert_eq!(expect, result);
    }

    #[test]
    fn type_check() {
        assert!(Type::atomic("A").is_atomic());
        assert!(Type::certificate("Alpha", "A").is_certificate());
        assert!(Type::proof("Alpha", "A").is_proof());

        assert!(!Type::atomic("A").is_certificate());
        assert!(!Type::atomic("A").is_proof());
        assert!(!Type::certificate("Alpha", "A").is_atomic());
        assert!(!Type::certificate("Alpha", "A").is_proof());
        assert!(!Type::proof("Alpha", "A").is_atomic());
        assert!(!Type::proof("Alpha", "A").is_certificate());
    }

    #[test]
    fn iterator_order() {
        let expected = vec![
            ("Certificate", "Epsilon"),
            ("Proof", "Delta"),
            ("Proof", "Gamma"),
            ("Certificate", "Beta"),
            ("Certificate", "Alpha"),
            ("Atomic", "A"),
        ];

        let _type = Type::atomic("A")
            .certify(Identity::from("Alpha"))
            .certify(Identity::from("Beta"))
            .prove(Identity::from("Gamma"))
            .prove(Identity::from("Delta"))
            .certify(Identity::from("Epsilon"));

        let it = _type.iter().map(|head| match head {
            Type::Certificate(id, _) => ("Certificate", id.as_str()),
            Type::Proof(id, _) => ("Proof", id.as_str()),
            Type::Atomic(a) => ("Atomic", a.as_str()),
        });
        let result: Vec<_> = it.collect();

        assert_eq!(expected, result);
    }

    #[test]
    fn iterator_tail_is_next_head() {
        let _type = Type::atomic("A")
            .certify(Identity::from("Alpha"))
            .certify(Identity::from("Beta"))
            .prove(Identity::from("Gamma"))
            .prove(Identity::from("Delta"))
            .certify(Identity::from("Epsilon"));

        let mut it = _type.iter();
        it.next().map(|mut head| {
            for result in it {
                head.iter().nth(1).map(|expect| {
                    assert_eq!(expect, result);
                    head = result;
                });
            }
        });
    }
}
