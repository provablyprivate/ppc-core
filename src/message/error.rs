use std::{
    error::Error,
    fmt,
};

use crate::{
    message::message::MessageBuilderError,
    Type,
};

/// Enum over things that can go wrong when handling messages.
#[derive(Debug)]
pub enum MessageError {
    FailedDecryption,
    FailedEncryption,
    FailedVerification,
    InvalidPossessionAttached,
    InvalidPossessionClaim,
    InvalidSignature,
    InvalidType(Type),
    KeyringError(Box<dyn Error>),
    MessageBuilderError(MessageBuilderError),
}

impl fmt::Display for MessageError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use MessageError::*;

        match self {
            FailedDecryption => write!(f, "decryption of message failed"),
            FailedEncryption => write!(f, "encryption of message failed"),
            FailedVerification => write!(f, "verification of message failed"),
            InvalidPossessionAttached => {
                write!(f, "attached invalid possession")
            }
            InvalidPossessionClaim => write!(f, "invalid possession in trace"),
            InvalidSignature => write!(f, "invalid signature for message"),
            InvalidType(_type) => {
                write!(f, "action not permitted for term of type {:?}", _type)
            }
            KeyringError(error) => write!(f, "keyring error, {}", error),
            MessageBuilderError(error) => write!(f, "builder error, {}", error),
        }
    }
}

impl Error for MessageError {}

impl From<MessageBuilderError> for MessageError {
    fn from(error: MessageBuilderError) -> MessageError {
        MessageError::MessageBuilderError(error)
    }
}
