use rand::{
    // Used to create random Agent ids and atomic types.
    distributions::Alphanumeric,
    Rng,
};

use super::{
    super::message::Message,
    MockKeyring,
};
use crate::{
    Identity,
    Term,
};

/// Random alphanumeric string of length 8.
pub fn atomic_label() -> String {
    let rng = rand::thread_rng();
    let atomic: String = rng
        .sample_iter(&Alphanumeric)
        .take(8)
        .map(char::from)
        .collect();
    atomic.to_uppercase()
}

/// Random atomic term.
pub fn atomic_term() -> Term<Vec<u8>> {
    let mut rng = rand::thread_rng();

    let d = rng.gen_range(0, 64) - 32;
    let body_size = 512 + d;
    let body = (0..body_size).map(|_| rand::random::<u8>()).collect();

    Term::atomic(atomic_label(), body)
}

/// Random term with several 'layers' of certificate and proofs.
pub fn term(keyring: &MockKeyring) -> Term<Vec<u8>> {
    let mut term = atomic_term();
    let mut rng = rand::thread_rng();

    for _ in 0..8 {
        let x = rng.gen_range(0.0, 1.0);
        if x > 0.9 {
            let id = keyring.arbitrary_secret_id();
            term = term.prove(keyring, id.clone()).unwrap().map(Vec::from);
        } else {
            let id = keyring.arbitrary_public_id();
            term = term.certify(keyring, id.clone()).unwrap().map(Vec::from);
        }
    }

    term
}

// Random valid message.
pub fn message(keyring: &MockKeyring) -> Message<Vec<u8>> {
    let receiver = keyring.arbitrary_public_id();
    message_with_receiver(&keyring, receiver.clone())
}

// Random valid message with set receiver.
pub fn message_with_receiver(
    keyring: &MockKeyring,
    id: Identity,
) -> Message<Vec<u8>> {
    let sender = keyring.arbitrary_secret_id();
    let term = term(keyring);
    Message::new(keyring, sender.clone(), id, term).unwrap()
}
