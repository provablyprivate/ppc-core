use std::collections::{
    HashMap,
    HashSet,
};

use rand::{
    seq::SliceRandom,
    CryptoRng,
    Rng,
    RngCore,
};

use crate::{
    identity::RandomIdentity,
    keyring::GeneralKeyringError,
    Identity,
    KeyQuartette,
    Keyring,
};

/// `MockKeyring` is a structure that implements a keyring that can be used
/// for testing.
pub struct MockKeyring {
    secret_ids: HashSet<Identity>,
    keys: HashMap<Identity, KeyQuartette>,
    exclude_public_keys: bool,
}

fn agent<R>(rng: &mut R) -> (Identity, KeyQuartette)
where
    R: RngCore + CryptoRng,
{
    let id = RandomIdentity::new();
    let keys = KeyQuartette::gen(rng);
    (id, keys)
}

fn choose<T>(a: Vec<&T>) -> &T {
    let mut rng = rand::thread_rng();
    a.choose(&mut rng).unwrap()
}

impl MockKeyring {
    fn secret_id_vector(&self) -> Vec<&Identity> {
        self.secret_ids.iter().collect()
    }

    fn public_id_vector(&self) -> Vec<&Identity> {
        self.keys
            .keys()
            .filter(|id| !self.secret_ids.contains(*id))
            .collect()
    }

    pub fn arbitrary_public_id(&self) -> &Identity {
        choose(self.public_id_vector())
    }

    pub fn arbitrary_secret_id(&self) -> &Identity {
        choose(self.secret_id_vector())
    }

    pub fn remove_secret_keys(&mut self, id: &Identity) {
        self.secret_ids.remove(id);
    }

    pub fn builder() -> MockKeyringConfigBuilder {
        MockKeyringConfigBuilder::default()
    }

    fn key<'a, F>(
        &'a self,
        id: &Identity,
        getter: F,
    ) -> Result<&[u8], GeneralKeyringError>
    where
        F: Fn(&'a KeyQuartette) -> &'a Vec<u8>,
    {
        self.keys
            .get_key_value(id)
            .map(|(id, _)| {
                self.keys.get(id).map(getter).map(Vec::as_slice).unwrap()
            })
            .ok_or(GeneralKeyringError::MissingIdentity)
    }

    fn public_key<'a, F>(
        &'a self,
        id: &Identity,
        getter: F,
    ) -> Result<&[u8], GeneralKeyringError>
    where
        F: Fn(&'a KeyQuartette) -> &'a Vec<u8>,
    {
        self.key(id, getter).and_then(|key| {
            if !self.exclude_public_keys || !self.secret_ids.contains(id) {
                Ok(key)
            } else {
                Err(GeneralKeyringError::MissingKey)
            }
        })
    }

    fn secret_key<'a, F>(
        &'a self,
        id: &Identity,
        getter: F,
    ) -> Result<&[u8], GeneralKeyringError>
    where
        F: Fn(&'a KeyQuartette) -> &'a Vec<u8>,
    {
        self.key(id, getter).and_then(|key| {
            if self.secret_ids.contains(id) {
                Ok(key)
            } else {
                Err(GeneralKeyringError::MissingKey)
            }
        })
    }
}

impl Keyring for MockKeyring {
    type Err = GeneralKeyringError;

    fn secret_encryption_bytes(
        &self,
        id: &Identity,
    ) -> Result<&[u8], Self::Err> {
        self.secret_key(id, |quartette| &quartette.secret_encryption_data)
    }

    fn public_encryption_bytes<'a>(
        &self,
        id: &Identity,
    ) -> Result<&[u8], Self::Err> {
        self.public_key(id, |quartette| &quartette.public_encryption_data)
    }

    fn secret_sign_bytes(&self, id: &Identity) -> Result<&[u8], Self::Err> {
        self.secret_key(id, |quartette| &quartette.secret_sign_data)
    }

    fn public_sign_bytes(&self, id: &Identity) -> Result<&[u8], Self::Err> {
        self.public_key(id, |quartette| &quartette.public_sign_data)
    }
}

// -----------------------------------------------------------------------------
// Builder
// -----------------------------------------------------------------------------

#[derive(Builder)]
pub struct MockKeyringConfig {
    #[builder(default = "0")]
    size: usize,

    #[builder(default = "0")]
    min_secret_ids: usize,

    #[builder(default = "0")]
    min_public_ids: usize,

    #[builder(default = "0.0")]
    secret_id_probability: f32,

    #[builder(default = "true")]
    exclude_public_keys: bool,
}

impl MockKeyringConfigBuilder {
    pub fn gen<R>(&mut self, rng: &mut R) -> MockKeyring
    where
        R: RngCore + CryptoRng,
    {
        let config = self.build().unwrap();

        let mut secret_ids = HashSet::new();
        let mut keys = HashMap::new();
        let mut size = config.size;

        for _ in 0..config.min_secret_ids {
            let (id, key_quartette) = agent(rng);
            keys.insert(id.clone(), key_quartette);
            secret_ids.insert(id);
            size -= 1;
        }

        for _ in 0..config.min_public_ids {
            let (id, key_quartette) = agent(rng);
            keys.insert(id.clone(), key_quartette);
            size -= 1;
        }

        for _ in 0..size {
            let (id, key_quartette) = agent(rng);
            keys.insert(id.clone(), key_quartette);
            if config.secret_id_probability > rng.gen_range(0.0, 1.0) {
                secret_ids.insert(id);
            }
        }

        MockKeyring {
            secret_ids,
            keys,
            exclude_public_keys: config.exclude_public_keys,
        }
    }
}
