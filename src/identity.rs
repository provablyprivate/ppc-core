use std::iter;

use rand::{
    distributions::Alphanumeric,
    rngs::ThreadRng,
    Rng,
};

/// Identifier for agents.
pub type Identity = String;

fn generate_alphanumeric(mut rng: ThreadRng) -> impl Iterator<Item = char> {
    iter::repeat(())
        .map(move |_| rng.sample(Alphanumeric))
        .map(char::from)
}

/// A random identity consisting of alphanumeric characters.
pub struct RandomIdentity;

impl RandomIdentity {
    /// Creates a random identity of lenth 16.
    pub fn new() -> Identity { RandomIdentity::sized(16) }

    /// Creates an identity with random alphanumeric characters of length
    /// `size`.
    pub fn sized(size: usize) -> Identity {
        generate_alphanumeric(rand::thread_rng())
            .take(size)
            .collect()
    }
}

/// A decorated identity that will most likely be unique.
///
/// The format of the identity is designed to be readable yet unique. The
/// pattern is as follows:
/// ```text
/// AAA_<label>_<padding>AAA
/// ```
/// Where `A` is a random alphanumeric character, `<label>` a human readable
/// string and `<padding>` a sequence of at least one alphanumeric character.
/// The length of a labeled identity is alway a multiple of 8.
///
/// # Example
/// Here are some identities generated with `LabeledIdentity`:
/// ```text
/// LabeledIdentity::new("Alpha")        ==> "c5i_Alpha_m50nPb"
/// LabeledIdentity::new("Beta")         ==> "Soo_Beta_opEGNz3"
/// LabeledIdentity::new("Website")      ==> "aaK_Website_L52I"
/// LabeledIdentity::new("ExampleLabel") ==> "rFX_ExampleLabel_JSxQ8ie"
/// LabeledIdentity::new("PogChamp")     ==> "Jz0_PogChamp_DFg0IgpiQJv"
/// ```
pub struct LabeledIdentity;

impl LabeledIdentity {
    /// Creates a new labeled identity containing the string `label`. Two
    /// identities created with the same label will most likely be unique.
    /// ```
    /// use ppc::identity::LabeledIdentity;
    ///
    /// let id_a = LabeledIdentity::new("Alpha");
    /// let id_b = LabeledIdentity::new("Alpha");
    ///
    /// // Both id_a and id_b contains the string 'Alpha'.
    /// assert!(id_a.contains("Alpha"));
    /// assert!(id_b.contains("Alpha"));
    ///
    /// // The chance of id_a being equal to id_b is small.
    /// assert_ne!(id_a, id_b);
    pub fn new(label: &str) -> Identity {
        let prefix: String =
            generate_alphanumeric(rand::thread_rng()).take(3).collect();

        let padding: String = generate_alphanumeric(rand::thread_rng())
            .take(8 - label.len() % 8)
            .collect();

        let suffix: String =
            generate_alphanumeric(rand::thread_rng()).take(3).collect();

        format!("{}_{}_{}{}", prefix, label, padding, suffix)
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    use super::*;

    /// There is nothing guaranteeing that a random identity is unique but if
    /// this test fails it could be considered not random enough.
    #[test]
    fn unique_random_identity() {
        let size = 512;
        let ids: HashSet<Identity> =
            (0..size).map(|_| RandomIdentity::new()).collect();

        assert_eq!(ids.len(), size);
    }

    /// There is nothing guaranteeing that a labeled identity is unique but if
    /// this test fails it could be considered not random enough.
    #[test]
    fn unique_labeled_identity() {
        let size = 512;
        let ids: HashSet<Identity> =
            (0..size).map(|_| LabeledIdentity::new("Alpha")).collect();

        assert_eq!(ids.len(), size);
    }

    #[test]
    fn labeled_identity() {
        // Empty label.

        let id = LabeledIdentity::new("");
        assert_eq!(id.chars().nth(3).unwrap(), '_');
        assert_eq!(id.chars().nth(4).unwrap(), '_');
        assert_eq!(id.len(), 16);

        let mut rng = rand::thread_rng();

        // Test invariance for labels with length not being a multiple of 8.

        let k = rng.gen_range(1, 8);

        let label: String = iter::repeat('@')
            .take(k * 8 - rng.gen_range(1, 8))
            .collect();

        let id = LabeledIdentity::new(&label);
        let len = id.len();

        assert_eq!(0, len % 8);
        assert_eq!(len / 8, k + 1);

        assert!(id.contains(&label));
        assert_eq!(id.chars().nth(3).unwrap(), '_');
        assert_eq!(id.chars().nth(4).unwrap(), '@');

        // Test invariance for labels with length being a multiple of 8.

        let k = rng.gen_range(1, 8);
        let label: String = iter::repeat('@').take(k * 8).collect();
        let id = LabeledIdentity::new(&label);
        let len = id.len();

        assert_eq!(0, len % 8);
        assert_eq!(len / 8, k + 2);

        assert!(id.contains(&label));
        assert_eq!(id.chars().nth(3).unwrap(), '_');
        assert_eq!(id.chars().nth(4).unwrap(), '@');
    }
}
