use std::collections::HashMap;

use super::abstract_syntax::*;
use crate::{
    Agent,
    Architecture,
};

pub fn eval_as(
    asppc: ASPPC,
) -> (
    Architecture,
    Vec<ASNegativeConstraint>,
    Vec<ASPositiveConstraint>,
) {
    let mut agents: HashMap<String, Agent> = HashMap::new();
    let mut architecture = Architecture::new();

    for agent_identity in asppc.agents() {
        let agent = Agent::empty(agent_identity);
        agents.insert(agent.clone_id(), agent);
    }

    // Build items, does nothing atm but will be used in typechecking
    for _item in asppc.items() {
        // Do something
    }

    // Build and add constructors to agents, return error if agent in
    // constructor is not declared
    for (id, content, args) in asppc.constructions() {
        match agents.get_mut(&id) {
            None => panic!("Agent not found"),
            Some(agent) => agent.add_constructor((content, args)),
        };
    }

    // Here we would do some typechecking on negative_constraints
    for _negative_constraint in asppc.negative_constraints() {
        // Do something
    }

    // Insert agents into architecture
    for (_, agent) in agents {
        architecture.insert_agent(agent);
    }

    (
        architecture,
        asppc.negative_constraints(),
        asppc.positive_constraints(),
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::Type;

    // Small test to check if parsing the demo input is successful. Does not
    // check the actual result of the parsing
    #[test]
    fn test_interpreting() {
        let mut asppc = ASPPC::new();

        asppc.add_item(Type::atomic("Info"));
        asppc.add_item(Type::atomic("Consent"));
        asppc.add_item(Type::atomic("Policy"));

        asppc.add_agent(String::from("Child"));
        asppc.add_agent(String::from("Website"));
        asppc.add_agent(String::from("Parent"));

        asppc.add_construction((
            String::from("Child"),
            Type::atomic("Info"),
            vec![],
        ));
        asppc.add_construction((
            String::from("Parent"),
            Type::atomic("Consent"),
            vec![],
        ));
        asppc.add_construction((
            String::from("Website"),
            Type::atomic("Policy"),
            vec![],
        ));

        asppc.add_negative_constraint((
            String::from("Website"),
            Type::atomic("Info"),
            String::from("Website"),
            Type::atomic("Consent"),
        ));
        asppc.add_negative_constraint((
            String::from("Website"),
            Type::atomic("Consent"),
            String::from("Parent"),
            Type::atomic("Policy"),
        ));

        asppc.add_positive_constraint((
            String::from("Website"),
            Type::atomic("Consent"),
        ));
        asppc.add_positive_constraint((
            String::from("Website"),
            Type::atomic("Info"),
        ));
        asppc.add_positive_constraint((
            String::from("Parent"),
            Type::atomic("Policy"),
        ));

        let (architecture, negative_constraints, positive_constraints) =
            eval_as(asppc);

        let mut expected_architecture = Architecture::new();

        // Expected agents and constructors
        let mut child = Agent::empty(String::from("Child"));
        child.add_constructor((Type::atomic("Info"), vec![]));
        expected_architecture.insert_agent(child);

        let mut website = Agent::empty(String::from("Website"));
        website.add_constructor((Type::atomic("Policy"), vec![]));
        expected_architecture.insert_agent(website);

        let mut parent = Agent::empty(String::from("Parent"));
        parent.add_constructor((Type::atomic("Consent"), vec![]));
        expected_architecture.insert_agent(parent);

        let expected_negative_constraints = vec![
            (
                String::from("Website"),
                Type::atomic("Info"),
                String::from("Website"),
                Type::atomic("Consent"),
            ),
            (
                String::from("Website"),
                Type::atomic("Consent"),
                String::from("Parent"),
                Type::atomic("Policy"),
            ),
        ];

        let expected_positive_constraints = vec![
            (String::from("Website"), Type::atomic("Consent")),
            (String::from("Website"), Type::atomic("Info")),
            (String::from("Parent"), Type::atomic("Policy")),
        ];

        assert_eq!(architecture, expected_architecture);
        assert_eq!(negative_constraints, expected_negative_constraints);
        assert_eq!(positive_constraints, expected_positive_constraints);
    }
}
