use crate::*;

pub type ASAgent = Identity;
pub type ASNegativeConstraint = (Identity, Type, Identity, Type);
pub type ASPositiveConstraint = (Identity, Type);
pub type ASConstruction = (Identity, Type, Vec<Type>);
pub type ASItem = Type;

/// This was supposed to be an AST but it ended up being just a collection of
/// data as a inbetween stage to reuse the evaluation for both the GUI and DSL
#[derive(Debug, PartialEq)]
pub struct ASPPC {
    agents: Vec<ASAgent>,
    items: Vec<ASItem>,
    negative_constraints: Vec<ASNegativeConstraint>,
    positive_constraints: Vec<ASPositiveConstraint>,
    constructions: Vec<ASConstruction>,
}

impl ASPPC {
    /// Creates an empty [[ASPPC]]
    pub fn new() -> Self {
        ASPPC {
            agents: Vec::new(),
            items: Vec::new(),
            negative_constraints: Vec::new(),
            positive_constraints: Vec::new(),
            constructions: Vec::new(),
        }
    }

    /// Adds an [[ASTAgent]] to the [[ASPPC]]
    pub fn add_agent(&mut self, agent: ASAgent) { self.agents.push(agent); }
    /// Adds an [[ASTNegativeConstraint]] to the [[ASPPC]]
    pub fn add_negative_constraint(
        &mut self,
        negative_constraint: ASNegativeConstraint,
    ) {
        self.negative_constraints.push(negative_constraint);
    }

    /// Adds an [[ASTPositiveConstraint]] to the [[ASPPC]]
    pub fn add_positive_constraint(
        &mut self,
        positive_constraint: ASPositiveConstraint,
    ) {
        self.positive_constraints.push(positive_constraint);
    }

    /// Adds an [[ASTConstruction]] to the [[ASPPC]]
    pub fn add_construction(&mut self, construction: ASConstruction) {
        self.constructions.push(construction);
    }
    /// Adds an [[ASTItem]] to the [[ASPPC]]
    pub fn add_item(&mut self, item: ASItem) { self.items.push(item); }

    pub fn agents(&self) -> Vec<ASAgent> { self.agents.clone() }
    pub fn items(&self) -> Vec<ASItem> { self.items.clone() }
    pub fn negative_constraints(&self) -> Vec<ASNegativeConstraint> {
        self.negative_constraints.clone()
    }
    pub fn positive_constraints(&self) -> Vec<ASPositiveConstraint> {
        self.positive_constraints.clone()
    }
    pub fn constructions(&self) -> Vec<ASConstruction> {
        self.constructions.clone()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_as_ppc() {
        let mut ast_ppc = ASPPC::new();
        ast_ppc.add_agent(String::from("Arkady"));
        ast_ppc.add_negative_constraint((
            String::from("Underhill"),
            Type::atomic("Bogdanov"),
            String::from("Mars"),
            Type::atomic("Ares"),
        ));
        ast_ppc.add_positive_constraint((
            String::from("Mars"),
            Type::atomic("Bogdanov"),
        ));
        ast_ppc.add_construction((
            String::from("Arkady"),
            Type::atomic("Bogdanov"),
            vec![],
        ));
        ast_ppc.add_item(Type::atomic("Bogdanov"));

        assert_eq!(
            ast_ppc,
            ASPPC {
                agents: vec![String::from("Arkady")],
                items: vec![Type::atomic("Bogdanov")],
                negative_constraints: vec![(
                    String::from("Underhill"),
                    Type::atomic("Bogdanov"),
                    String::from("Mars"),
                    Type::atomic("Ares"),
                )],
                positive_constraints: vec![(
                    String::from("Mars"),
                    Type::atomic("Bogdanov")
                )],
                constructions: vec![(
                    String::from("Arkady"),
                    Type::atomic("Bogdanov"),
                    vec![],
                )],
            }
        );
    }
}
