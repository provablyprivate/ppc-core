use std::collections::HashMap;

use super::abstract_syntax::ASPPC;
use crate::{
    Identity,
    Type,
};

//Typechecks an ASPPC structure, verifying item and agent declarations and
// agent-item possessions for message routes and constraints.
pub fn typecheck_as(asppc: &ASPPC) {
    let mut items: Vec<Type> = Vec::new();
    let mut agents: HashMap<Identity, Vec<Type>> = HashMap::new();
    //Iterate through each of the different vectors containing the different
    // declarations and typecheck them in order.

    //Typecheck all item declarations.
    for item in asppc.items() {
        match typecheck_item(item, items) {
            Ok(return_items) => {
                items = return_items;
            }
            Err((error_message, failed_item)) => {
                panic!("{} in item {:?}", error_message, failed_item);
            }
        }
    }

    //Typecheck all agent declarations
    for agent_identity in asppc.agents() {
        match typecheck_agent(agent_identity, agents) {
            Ok(return_agents) => {
                agents = return_agents;
            }
            Err((error_message, failed_agent)) => {
                panic!("{} in agent {}", error_message, failed_agent);
            }
        }
    }

    //Typecheck all construction declarations
    for (id, content, _args) in asppc.constructions() {
        match typecheck_construct(id, content, &items, agents) {
            Ok(return_agents) => {
                agents = return_agents;
            }
            Err((error_message, constructing_agent, constructed_item)) => {
                panic!(
                    "{} in construct '{} : {:?}'",
                    error_message, constructing_agent, constructed_item
                );
            }
        }
    }

    //Verify all negative constraints, they are checked to make sure agents are
    // declared, and items can be acquired by agents.
    for (is_constrained, i_constrained, constrained_by, constraining_i) in
        asppc.negative_constraints()
    {
        //Temp_item is used for error_message clarity only.
        let i_constrained_clone = i_constrained.clone();
        //Validates possession for both agents, although this might change.
        match typecheck_positive_constraint(
            is_constrained.clone(),
            i_constrained,
            &agents,
            &items,
        ) {
            Ok(_) => {}
            Err((error_message, cons_agent, cons_item)) => {
                panic!(
                    "Constrained {} in negative constraint '{} - {:?} => {} - \
                     {:?}'",
                    error_message,
                    cons_agent,
                    cons_item,
                    constrained_by,
                    constraining_i
                );
            }
        }
        match typecheck_positive_constraint(
            constrained_by,
            constraining_i,
            &agents,
            &items,
        ) {
            Ok(_) => {}
            Err((error_message, cons_agent, cons_item)) => {
                panic!(
                    "Constraining {} in negative constraint '{} - {:?} => {} \
                     - {:?}'",
                    error_message,
                    is_constrained,
                    i_constrained_clone,
                    cons_agent,
                    cons_item
                );
            }
        }
    }

    //Verifying positive constraints.
    for (agent, item) in asppc.positive_constraints() {
        //Possible expansion, add circular dependency checks ONLY on positive
        // constraints, with few positive constraints we can simply
        // construct a valid trace of each positive constraints which
        // would add extra verification of these specific constraints.
        match typecheck_positive_constraint(agent, item, &agents, &items) {
            Ok(_) => {}
            Err((error_message, cons_agent, cons_item)) => {
                panic!(
                    "{} in positive constraint {}: {:?}",
                    error_message, cons_agent, cons_item
                );
            }
        }
    }
}

//if any of the typechecking functions fail, it returns an error in the form of
//(String, Agent, Item) (or in some cases, only (String, Agent) or (String,
//(String, Item)
//in the cases where relevant). This is then used to panic with useful error
// messages.

//Verify that the item has not already been declared, in that case, add it to
// the list of known items.
fn typecheck_item(
    item: Type,
    mut items: Vec<Type>,
) -> Result<Vec<Type>, (String, Type)> {
    if !(items.contains(&item)) {
        items.push(item);
        Ok(items)
    } else {
        Err((String::from("Item declared twice"), item))
    }
}

//Verify that the agent has not already been declared, in that case, add it to
// the list of known agents.
fn typecheck_agent(
    agent: Identity,
    mut agents: HashMap<Identity, Vec<Type>>,
) -> Result<HashMap<Identity, Vec<Type>>, (String, Identity)> {
    if !(agents.contains_key(&agent)) {
        agents.insert(agent, Vec::new());
        Ok(agents)
    } else {
        Err((String::from("Agent declared twice"), agent))
    }
}

//Verify that all agents and items have been declared, in that case, add the
// item to the list of agent possessions (Defined as the Vec<Type> of the
// HashMap of the agents).
fn typecheck_construct(
    agent: Identity,
    item: Type,
    items: &Vec<Type>,
    mut agents: HashMap<Identity, Vec<Type>>,
) -> Result<HashMap<Identity, Vec<Type>>, (String, Identity, Type)> {
    if !(agents.contains_key(&agent)) {
        Err((String::from("Agent not declared"), agent, item))
    } else if !(items.contains(&item)) {
        Err((String::from("Item not declared"), agent, item))
    } else {
        let mut old_items: Vec<Type> = agents.get_mut(&agent).unwrap().to_vec();
        old_items.push(item);
        agents.insert(agent, old_items.to_vec());
        Ok(agents)
    }
}

//Verify that the agent of the constraint exists, and that it can possess the
// item in the constraint. This is used in several different constraint checks,
// twice for the negative constraint checks, and once for the positive
// constraint checks.
fn typecheck_positive_constraint(
    agent: Identity,
    item: Type,
    agents: &HashMap<Identity, Vec<Type>>,
    items: &Vec<Type>,
) -> Result<(), (String, Identity, Type)> {
    let constrained_agent = agents.get(&agent);
    match constrained_agent {
        None => Err((String::from("Agent not declared"), agent, item)),
        Some(_) => {
            if !(items.contains(&item)) {
                Err((String::from("Item not declared"), agent, item))
            } else {
                Ok(())
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    //Some tests using the 'should panic' tag can be extended to verify the
    // panic message as well, although as there is still some possible
    // changes, the message is not yet set in stone, and as such, currently
    // the check will only be a verification that the typechecker panics.
    // Another alternative is for the typechecker to never panic and instead
    // simply return a message which is then asserted, or even collect all
    // type-errors and return all of them.

    #[test]
    fn test_typecheck_item_add() {
        let mut asppc: ASPPC = ASPPC::new();
        asppc.add_item(Type::atomic("Item1"));
        asppc.add_item(Type::atomic("Item2"));
        typecheck_as(&asppc);
    }

    #[test]
    #[should_panic]
    fn test_typecheck_item_add_replica() {
        let mut asppc: ASPPC = ASPPC::new();
        asppc.add_item(Type::atomic("Item1"));
        asppc.add_item(Type::atomic("Item1"));
        typecheck_as(&asppc);
    }

    #[test]
    fn test_typecheck_agent_add() {
        let mut asppc: ASPPC = ASPPC::new();
        asppc.add_agent(Identity::from("Agent1"));
        asppc.add_agent(Identity::from("Agent2"));
        typecheck_as(&asppc);
    }

    #[test]
    #[should_panic]
    fn test_typecheck_agent_add_replica() {
        let mut asppc: ASPPC = ASPPC::new();
        asppc.add_agent(Identity::from("Agent1"));
        asppc.add_agent(Identity::from("Agent1"));
        typecheck_as(&asppc);
    }

    #[test]
    fn test_typecheck_construct() {
        let mut asppc: ASPPC = ASPPC::new();
        asppc.add_agent(Identity::from("Agent1"));
        asppc.add_item(Type::atomic("Item1"));
        asppc.add_construction((
            Identity::from("Agent1"),
            Type::atomic("Item1"),
            vec![],
        ));
        typecheck_as(&asppc);
    }

    #[test]
    #[should_panic]
    fn test_typecheck_construct_no_item() {
        let mut asppc: ASPPC = ASPPC::new();
        asppc.add_agent(Identity::from("Agent1"));
        asppc.add_construction((
            Identity::from("Agent1"),
            Type::atomic("Item1"),
            vec![],
        ));
        typecheck_as(&asppc);
    }

    #[test]
    #[should_panic]
    fn test_typecheck_construct_no_agent() {
        let mut asppc: ASPPC = ASPPC::new();
        asppc.add_item(Type::atomic("Item1"));
        asppc.add_construction((
            Identity::from("Agent1"),
            Type::atomic("Item1"),
            vec![],
        ));
        typecheck_as(&asppc);
    }

    #[test]
    fn test_typecheck_negative_constraint() {
        let mut asppc: ASPPC = ASPPC::new();
        asppc.add_agent(Identity::from("Agent1"));
        asppc.add_agent(Identity::from("Agent2"));
        asppc.add_item(Type::atomic("Item1"));
        asppc.add_item(Type::atomic("Item2"));
        asppc.add_construction((
            Identity::from("Agent1"),
            Type::atomic("Item1"),
            vec![],
        ));
        asppc.add_construction((
            Identity::from("Agent2"),
            Type::atomic("Item2"),
            vec![],
        ));
        asppc.add_negative_constraint((
            Identity::from("Agent1"),
            Type::atomic("Item1"),
            Identity::from("Agent2"),
            Type::atomic("Item2"),
        ));
        typecheck_as(&asppc);
    }

    #[test]
    #[should_panic]
    fn test_typecheck_negative_constraint_no_item() {
        let mut asppc: ASPPC = ASPPC::new();
        asppc.add_agent(Identity::from("Agent1"));
        asppc.add_agent(Identity::from("Agent2"));
        asppc.add_item(Type::atomic("Item2"));
        asppc.add_construction((
            Identity::from("Agent2"),
            Type::atomic("Item2"),
            vec![],
        ));
        asppc.add_negative_constraint((
            Identity::from("Agent1"),
            Type::atomic("Item1"),
            Identity::from("Agent2"),
            Type::atomic("Item2"),
        ));
        typecheck_as(&asppc);
    }

    #[test]
    #[should_panic]
    fn test_typecheck_negative_constraint_no_agent() {
        let mut asppc: ASPPC = ASPPC::new();
        asppc.add_agent(Identity::from("Agent2"));
        asppc.add_item(Type::atomic("Item1"));
        asppc.add_item(Type::atomic("Item2"));
        asppc.add_construction((
            Identity::from("Agent2"),
            Type::atomic("Item2"),
            vec![],
        ));
        asppc.add_negative_constraint((
            Identity::from("Agent1"),
            Type::atomic("Item1"),
            Identity::from("Agent2"),
            Type::atomic("Item2"),
        ));
        typecheck_as(&asppc);
    }

    //Positive constraint testing is currently not necessary as it uses the
    // same function as negative constraint.
}
