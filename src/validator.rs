use core::panic;
use std::collections::HashMap;

use crate::{
    keycollection::KeyCollection,
    message::{
        Message,
        MessageError,
    },
    Agent,
    Architecture,
    Certificate,
    Identity,
    Keyring,
    Proof,
    Term,
    Type,
};

// Keys implemented as Strings

pub struct Validator<T: Keyring> {
    my_id: Identity,
    architecture: Architecture,
    keyring: T,
    possessions: Vec<Message<()>>,
}

impl Validator<KeyCollection> {
    pub fn new(
        my_id: Identity,
        secret_sign: Vec<u8>,
        secret_encrypt: Vec<u8>,
    ) -> Validator<KeyCollection> {
        Self {
            my_id: Identity::from(my_id.as_str()),
            architecture: Architecture::new(),
            keyring: KeyCollection::new(my_id, secret_sign, secret_encrypt),
            possessions: Vec::new(),
        }
    }

    //----------------------------------------------
    //############ Architecture Related ############
    //----------------------------------------------

    pub fn insert_agent(
        &mut self,
        agent: Agent,
        enc_key: Vec<u8>,
        sign_key: Vec<u8>,
    ) {
        self.keyring.insert_agent(agent.clone(), enc_key, sign_key);
        self.architecture.insert_agent(agent);
    }

    pub fn has_possession(&self, _type: Type) -> Message<()> {
        for msg in &self.possessions {
            if &_type == msg.term(&self.keyring).unwrap()._type() {
                return msg.clone();
            }
        }

        panic!("Could not find a possesion of type: {:?}", _type);
    }

    //-----------------------------------------
    //############ Atomic Messages ############
    //-----------------------------------------

    pub fn create_atomic_base_message(
        &self,
        receiver: Identity,
        atomic_type: String,
        body: Vec<u8>,
    ) -> Message<Vec<u8>> {
        // create placeholder type to search constructor for an atomic
        // constructor matching the given atomic type
        let eventual_type = Term::atomic(atomic_type.into(), body);
        // This panics if either the constructor result does not exist or if
        // there are required types to create the given message
        if !self
            .get_agents_constructors(self.my_id.clone())
            .get(&eventual_type._type())
            .unwrap()
            .is_empty()
        {
            panic!(
                "Either you can not create this type or the type is not atomic"
            )
        }
        Message::new(&self.keyring, self.my_id.clone(), receiver, eventual_type)
            .unwrap()
    }

    pub fn create_atomic_message(
        &self,
        receiver: Identity,
        atomic_type: String,
        body: Vec<u8>,
    ) -> Message<Vec<u8>> {
        let mut potential_message = Message::new(
            &self.keyring,
            self.my_id.clone(),
            receiver,
            Term::atomic(atomic_type.clone(), body),
        )
        .unwrap();
        for required in self
            .get_agents_constructors(self.my_id.clone())
            .get(Term::atomic(atomic_type.clone(), ())._type())
            .unwrap()
        {
            potential_message
                .attach(&self.keyring, self.has_possession(required.clone()))
                .unwrap();
        }

        potential_message
    }

    pub fn validate_atomic_message(&self, message: Message<()>) {
        // Validate all keys
        message.as_ok(&self.keyring).unwrap();
        // Checks that the current message could be constructed using the
        // messages in it trait, only one level down, but recursing takes
        // care of lower levels
        self.check_type_constructable(&message);
        if !message.trace(&self.keyring).unwrap().is_empty() {
            // If trace is not empty Look through the trace recursivly to secure
            // that all agents have the correct constructors
            for trace_message in message.trace(&self.keyring).unwrap() {
                self.validate_atomic_message(trace_message.clone());
            }
        }

        // Look through the trace recursivly to secure that all agents have the
        // correct constructors
    }

    // Used to check if a message contains all its neccessary components in its
    // trace
    fn check_type_constructable(&self, message: &Message<()>) {
        // Collect all the needed types that the sender would need to construct
        // the type
        let needed_types = self
            .get_agents_constructors(Identity::from(
                message.sender(&self.keyring).unwrap(),
            ))
            .get(message.term(&self.keyring).unwrap()._type())
            .unwrap()
            .clone();

        // Collect all the types of the messages that are in the trace
        let message_trace_types: Vec<&Type> = message
            .trace(&self.keyring)
            .unwrap()
            .iter()
            .map(|msg| msg.term(&self.keyring).unwrap()._type())
            .collect();

        // If we got all the needed types in the trace we assume the the
        // constructing agent could have contructed it. This function
        // needs to be used together with checking the sign of all messages,
        // which validate_atomic_message does
        for needed_type in needed_types {
            if !message_trace_types.contains(&&needed_type) {
                panic!(
                    "Message is not valid, trace does not contain messages to \
                     satisfy the messages constructor: {}",
                    message
                        .term(&self.keyring)
                        .unwrap()
                        ._type()
                        .get_type_string()
                )
            }
        }
    }

    //-----------------------------------------------
    //############ Message Certification ############
    //-----------------------------------------------

    pub fn certify_message(
        &self,
        message: Message<Vec<u8>>,
    ) -> Message<Certificate> {
        if !message.term(&self.keyring).unwrap()._type().is_atomic() {
            panic!(
                "Passed message is not of type atomic, you can only create \
                 certificates from atomics even if the given inner type is \
                 Vec<u8>"
            )
        };

        Message::new(
            &self.keyring,
            message.sender(&self.keyring).unwrap().into(),
            message.receiver(&self.keyring).unwrap().into(),
            Term::certify(
                message.term(&self.keyring).unwrap().clone(),
                &self.keyring,
                message.receiver(&self.keyring).unwrap().to_string(),
            )
            .unwrap(),
        )
        .unwrap()
    }

    pub fn decertify_message(
        &self,
        message: Message<Certificate>,
    ) -> Message<Vec<u8>> {
        Message::new(
            &self.keyring,
            message.sender(&self.keyring).unwrap().into(),
            message.receiver(&self.keyring).unwrap().into(),
            Term::decertify(
                message.term(&self.keyring).unwrap().clone(),
                &self.keyring,
            )
            .unwrap(),
        )
        .unwrap()
    }

    //----------------------------------------
    //############ Message Proofs ############
    //----------------------------------------

    pub fn create_proof(&self, message: Message<Vec<u8>>) -> Message<Proof> {
        if !message.term(&self.keyring).unwrap()._type().is_atomic() {
            panic!(
                "Passed message is not of type atomic, you can only create \
                 proof from atomics even if the given inner type is Vec<u8>"
            )
        };

        Message::new(
            &self.keyring,
            message.sender(&self.keyring).unwrap().into(),
            message.receiver(&self.keyring).unwrap().into(),
            Term::prove(
                message.term(&self.keyring).unwrap().clone(),
                &self.keyring,
                message.receiver(&self.keyring).unwrap().to_string(),
            )
            .unwrap(),
        )
        .unwrap()
    }

    pub fn verify_proof(
        &self,
        message: Message<Proof>,
    ) -> Result<(), MessageError> {
        message
            .term(&self.keyring)
            .unwrap()
            .clone()
            .verify(&self.keyring)
    }

    //--------------------------------------
    //############ Constructors ############
    //--------------------------------------

    pub fn get_constructors(&self) -> HashMap<Type, Vec<Type>> {
        self.architecture
            .get_agents()
            .get(&self.my_id)
            .unwrap()
            .get_constructors()
    }

    pub fn get_constructors_clone(&self) -> HashMap<Type, Vec<Type>> {
        self.architecture
            .get_agents()
            .get(&self.my_id)
            .unwrap()
            .get_constructors()
            .clone()
    }

    pub fn get_agents_constructors(
        &self,
        id: Identity,
    ) -> HashMap<Type, Vec<Type>> {
        self.architecture
            .get_agents()
            .get(&id)
            .unwrap()
            .get_constructors()
    }

    pub fn get_constructable_types(&self) -> Vec<String> {
        let mut res: Vec<String> = Vec::new();

        for term in self
            .architecture
            .get_agents()
            .get(&self.my_id)
            .unwrap()
            .get_constructors()
            .keys()
        {
            res.push(term.get_type_string());
        }

        res
    }

    pub fn get_agents_constructable_types(&self, id: Identity) -> Vec<String> {
        let mut res: Vec<String> = Vec::new();
        for term in self
            .architecture
            .get_agents()
            .get(&id)
            .unwrap()
            .get_constructors()
            .keys()
        {
            res.push(term.get_type_string());
        }
        res
    }

    pub fn add_possesion(&mut self, message: Message<()>) {
        self.possessions.push(message);
    }

    pub fn construct_message(&self) {}
}

//-------------------------------
//############ TESTS ############
//-------------------------------

#[cfg(test)]
mod tests {

    use crate::{
        agent::ConstructorBuilder,
        keycollection::KeyCollection,
        Term,
    };
    use crate::{
        validator::Validator,
        Message,
    };
    use crate::{
        Agent,
        Identity,
        KeyQuartette,
    };

    // So that the init of a Validator is not needed in every test function
    fn get_test_env() -> Validator<KeyCollection> {
        // Generate the keys
        let mut rng = rand::thread_rng();
        let keys = KeyQuartette::gen(&mut rng);

        // Create the validator instance
        let mut validator = Validator::new(
            Identity::from("agent1"),
            keys.secret_sign_data,
            keys.secret_encryption_data,
        );

        // constructors for create_atomic_message_base_test()
        let camt_res_type =
            Term::atomic("Result".into(), "Body".as_bytes().to_vec());

        let camt_constructor = ConstructorBuilder::new()
            .result(camt_res_type._type().clone())
            .build()
            .unwrap();

        // constructors for check_type_constructable_test()
        let ctc_res_type =
            Term::atomic("Resultctc".into(), "Bodyctc".as_bytes().to_vec());
        let ctc_arg_type =
            Term::atomic("Argctc".into(), "Bodyctc".as_bytes().to_vec());

        let ctc_constructor = ConstructorBuilder::new()
            .result(ctc_res_type._type().clone())
            .arg(ctc_arg_type._type().clone())
            .build()
            .unwrap();

        // constructors for validate_atomic_message_test()
        let vam_arg_type =
            Term::atomic("Argvam".into(), "Bodyvam".as_bytes().to_vec());

        let vam_constructor = ConstructorBuilder::new()
            .result(ctc_arg_type._type().clone())
            .arg(vam_arg_type._type().clone())
            .build()
            .unwrap();

        let vam_base_constructor = ConstructorBuilder::new()
            .result(vam_arg_type._type().clone())
            .build()
            .unwrap();

        // Create agent and give it the instructor, then insert it into the
        // architecture
        let mut agent = Agent::empty(Identity::from("agent1"));
        agent.add_constructor(camt_constructor);
        agent.add_constructor(ctc_constructor);
        agent.add_constructor(vam_constructor);
        agent.add_constructor(vam_base_constructor);

        validator.insert_agent(
            agent,
            keys.public_encryption_data,
            keys.public_sign_data,
        );

        // Define second agent, the one who will be the receiver for all
        // messages sent to check validity
        let agent = Agent::empty(Identity::from("agent2"));

        let mut rng = rand::thread_rng();
        let keys = KeyQuartette::gen(&mut rng);

        validator.insert_agent(
            agent,
            keys.public_encryption_data,
            keys.public_sign_data,
        );

        // Return a validator with two agents, agent1 and agent2. Agent1 has all
        // constructors in the tests since we need the privatekeys

        validator
    }

    ////////////////////////////////////////////////////////

    #[test]
    fn create_atomic_base_message_test() {
        let validator = get_test_env();

        // Create a message using
        let created_message = validator
            .create_atomic_base_message(
                Identity::from("agent2"),
                "Result".to_string(),
                "Body".as_bytes().to_vec(),
            )
            .term(&validator.keyring)
            .unwrap()
            ._type()
            .clone();

        let res_type =
            Term::atomic("Result".into(), "Body".as_bytes().to_vec());

        assert_eq!(res_type._type().clone(), created_message);
    }

    //////////////////////////////////////////

    #[test]
    fn check_type_constructable_test() {
        let validator = get_test_env();

        // This would be a created messaged passed from another agent, therefore
        // we need to create it outside of normal restrictions since the
        // constructor would not be satisfied
        let mut message_res = Message::new(
            &validator.keyring,
            "agent1".into(),
            "agent2".into(),
            Term::atomic("Resultctc".into(), "Bodyctc".as_bytes().to_vec()),
        )
        .unwrap()
        .into_attachment();

        let message_trace_arg = Message::new(
            &validator.keyring,
            "agent1".into(),
            "agent1".into(),
            Term::atomic("Argctc".into(), "Bodyctc".as_bytes().to_vec()),
        )
        .unwrap()
        .into_attachment();
        message_res
            .attach(&validator.keyring, message_trace_arg)
            .unwrap();

        validator.check_type_constructable(&message_res);
    }

    ////////////////////////////

    #[test]
    fn validate_atomic_message_test() {
        let validator = get_test_env();

        let mut message_res = Message::new(
            &validator.keyring,
            "agent1".into(),
            "agent2".into(),
            Term::atomic("Resultctc".into(), "Bodyctc".as_bytes().to_vec()),
        )
        .unwrap()
        .into_attachment();

        let mut message_trace_arg = Message::new(
            &validator.keyring,
            "agent1".into(),
            "agent1".into(),
            Term::atomic("Argctc".into(), "Bodyctc".as_bytes().to_vec()),
        )
        .unwrap()
        .into_attachment();

        let message_trace_arg2 = Message::new(
            &validator.keyring,
            "agent1".into(),
            "agent1".into(),
            Term::atomic("Argvam".into(), "Bodyvam".as_bytes().to_vec()),
        )
        .unwrap()
        .into_attachment();

        message_trace_arg
            .attach(&validator.keyring, message_trace_arg2)
            .unwrap();
        message_res
            .attach(&validator.keyring, message_trace_arg)
            .unwrap();

        validator.validate_atomic_message(message_res);
    }

    ////////////////////////////

    #[test]
    fn certify_decertify_message_test() {
        let validator = get_test_env();

        let base_message = validator.create_atomic_base_message(
            Identity::from("agent1"),
            "Result".into(),
            "Body".into(),
        );

        let certified_message = validator.certify_message(base_message);

        let decertified_message =
            validator.decertify_message(certified_message);

        assert_eq!(
            "Body",
            std::str::from_utf8(
                decertified_message.term(&validator.keyring).unwrap().body()
            )
            .unwrap()
        )
    }

    #[test]
    fn prove_validate_message_test() {
        let validator = get_test_env();

        let base_message = validator.create_atomic_base_message(
            Identity::from("agent1"),
            "Result".into(),
            "Body".into(),
        );

        let proof_message = validator.create_proof(base_message);

        validator.verify_proof(proof_message).unwrap();
    }

    #[test]
    fn create_atomic_message_test() {
        let mut validator = get_test_env();

        let argctc_possession_msg = Message::builder()
            .sender(validator.my_id.clone())
            .receiver(validator.my_id.clone())
            .term(Term::atomic(
                "Argctc".into(),
                "Irrelevant".as_bytes().to_vec(),
            ))
            .sign(&validator.keyring)
            .unwrap();

        let argctc_possession = argctc_possession_msg.into_attachment();

        validator.add_possesion(argctc_possession);

        validator.create_atomic_message(
            Identity::from("agent2"),
            "Resultctc".into(),
            "irrelevant".into(),
        );
    }
}
