use super::participant;
use crate::{
    participant::ParticipantError,
    Identity,
    Keyring,
    Message,
    Participant,
    PossessionHandler,
    Term,
};

#[derive(Clone)]
enum Layer {
    Proof(Identity),
    Certificate(Identity),
    Decertify,
}

/// Descriptor for creating `Message<T>` as a participant.
#[derive(Builder)]
#[builder(pattern = "owned")]
pub struct ParticipantMessageDescriptor<'a, K, P, T>
where
    K: Keyring,
    P: PossessionHandler,
    T: Clone,
{
    #[builder(private, default)]
    participant: Option<&'a mut Participant<K, P>>,

    sender: Identity,
    receiver: Identity,
    term: Term<T>,

    #[builder(private, default = "Vec::new()")]
    layers: Vec<Layer>,
}

impl<'a, K, P> ParticipantMessageDescriptorBuilder<'a, K, P, Vec<u8>>
where
    K: Keyring,
    P: PossessionHandler,
    P::Body: From<Vec<u8>>,
{
    /// Create a new instance of `ParticipantMessageDescriptorBuilder` that has
    /// a reference to a `Participant`.
    pub fn new(
        participant: &'a mut Participant<K, P>,
    ) -> ParticipantMessageDescriptorBuilder<'a, K, P, Vec<u8>> {
        let builder = ParticipantMessageDescriptorBuilder::default();
        builder.participant(Some(participant))
    }

    /// Turns the message into a proof of the current type for the agent
    /// associated with `id`.
    pub fn prove(
        mut self,
        id: Identity,
    ) -> ParticipantMessageDescriptorBuilder<'a, K, P, Vec<u8>> {
        let layers = self.layers.get_or_insert(Vec::new());
        layers.push(Layer::Proof(id));
        self
    }

    /// Turns the message into a certificate of the current term for the agent
    /// associated with `id`.
    pub fn certify(
        mut self,
        id: Identity,
    ) -> ParticipantMessageDescriptorBuilder<'a, K, P, Vec<u8>> {
        let layers = self.layers.get_or_insert(Vec::new());
        layers.push(Layer::Certificate(id));
        self
    }

    /// Decertifies a certificate.
    pub fn decertify(
        mut self,
    ) -> ParticipantMessageDescriptorBuilder<'a, K, P, Vec<u8>> {
        let layers = self.layers.get_or_insert(Vec::new());
        layers.push(Layer::Decertify);
        self
    }

    /// The term of `message` will be used as the root argument for the builder.
    pub fn argument(
        self,
        message: Message<Vec<u8>>,
    ) -> Result<
        ParticipantMessageDescriptorBuilder<'a, K, P, Vec<u8>>,
        ParticipantError,
    > {
        let participant = &self.participant;
        let participant = participant.as_ref().unwrap().as_ref().unwrap();
        let term = message
            .into_term(participant::keyring(participant))?
            .clone();
        Ok(self.term(term))
    }

    /// Finalizes the builder and generated a `Message<Vec<u8>>` on success. The
    /// funtion fails if the participant doesn't possess the required arguments
    /// or keys or if the message is invalid within the architecture.
    ///
    /// ## NOTE:
    /// This function should be called instead of `build()`.
    pub fn sign(self) -> Result<Message<Vec<u8>>, ParticipantError> {
        let descriptor = self.build()?;
        let keyring =
            participant::keyring(descriptor.participant.as_ref().unwrap());
        let term: Term<Vec<u8>> = descriptor
            .layers
            .into_iter()
            .rev()
            .try_fold(descriptor.term, |term, layer| match layer {
                Layer::Proof(id) => {
                    term.prove(keyring, id).map(|term| term.map(Vec::from))
                }
                Layer::Certificate(id) => {
                    term.certify(keyring, id).map(|term| term.map(Vec::from))
                }
                Layer::Decertify => term.decertify(keyring),
            })?;
        let message = Message::builder()
            .sender(descriptor.sender)
            .receiver(descriptor.receiver)
            .term(term)
            .sign(keyring)?;
        participant::attach_valid_trace(
            descriptor.participant.unwrap(),
            message,
        )
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use super::*;
    use crate::{
        agent::ConstructorBuilder,
        Agent,
        Architecture,
        KeyQuartette,
        Type,
    };

    #[test]
    fn build_message() {
        //
        // Setup the participant.
        //

        let mut architecture = Architecture::new();

        let mut agent_b = agent!();

        agent_b.add_constructor(
            ConstructorBuilder::new()
                .arg(Type::certificate(agent_b.id(), "QUOTE"))
                .result(Type::atomic("QUOTE"))
                .build()
                .unwrap(),
        );
        agent_b.add_constructor(
            ConstructorBuilder::new()
                .arg(Type::atomic("QUOTE"))
                .result(Type::proof(agent_b.id(), "QUOTE"))
                .build()
                .unwrap(),
        );

        let id_b = architecture.insert_agent(agent_b);
        let id_a = architecture.insert_agent(agent!(
            => Type::atomic("QUOTE"),
            Type::atomic("QUOTE") => Type::certificate(&id_b, "QUOTE")));
        let i_id_b =
            architecture.insert_input_interface(Agent::empty(id_b.clone()));
        let i_id_a =
            architecture.insert_input_interface(Agent::empty(id_a.clone()));
        let o_id_b =
            architecture.insert_output_interface(Agent::empty(id_b.clone()));
        let o_id_a =
            architecture.insert_output_interface(Agent::empty(id_a.clone()));

        let mut rng = rand::thread_rng();
        let keyring: HashMap<_, _> = vec![
            (id_a.clone(), KeyQuartette::gen(&mut rng)),
            (id_b.clone(), KeyQuartette::gen(&mut rng)),
            (i_id_a.clone(), KeyQuartette::gen(&mut rng)),
            (i_id_b.clone(), KeyQuartette::gen(&mut rng)),
            (o_id_a.clone(), KeyQuartette::gen(&mut rng)),
            (o_id_b.clone(), KeyQuartette::gen(&mut rng)),
        ]
        .into_iter()
        .collect();

        let possesion_handler: HashMap<Type, Vec<Message<Vec<u8>>>> =
            HashMap::new();

        let mut participant =
            Participant::new(architecture, keyring, Some(possesion_handler));

        // Create a message containing atomic data.
        let data = "i think everybody should like everybody"
            .as_bytes()
            .to_vec();
        let atomic = participant
            .build_message()
            .sender(id_a.clone())
            .receiver(id_a.clone())
            .term(Term::atomic(String::from("QUOTE"), data.clone()))
            .sign()
            .unwrap();

        // Create a certificate from the message.
        let certificate = participant
            .build_message()
            .sender(id_a.clone())
            .receiver(id_b.clone())
            .argument(atomic)
            .unwrap()
            .certify(id_b.clone())
            .sign()
            .unwrap();

        let _type = certificate
            .term(participant::keyring(&participant))
            .unwrap()
            ._type();
        assert_eq!(_type, &Type::certificate(&id_b, "QUOTE"));

        let decertified = participant
            .build_message()
            .sender(id_b.clone())
            .receiver(id_b.clone())
            .argument(certificate)
            .unwrap()
            .decertify()
            .sign()
            .unwrap();

        // The decertified data should be equal to the original data.
        assert_eq!(
            decertified
                .term(participant::keyring(&participant))
                .unwrap()
                .body(),
            &data
        );

        // Create a proof of the decertified message.
        let proof = participant
            .build_message()
            .sender(id_b.clone())
            .receiver(id_b.clone())
            .argument(decertified)
            .unwrap()
            .prove(id_b)
            .sign()
            .unwrap();

        // Verify that the proof is correct.
        participant.verify(proof).unwrap();
    }
}
