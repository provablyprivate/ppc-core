use std::{
    error::Error,
    fmt,
};

use crate::{
    message::MessageError,
    participant::message_builder::ParticipantMessageDescriptorBuilderError,
    Identity,
    Type,
};

#[derive(Debug)]
pub enum ParticipantError {
    NonePossessionHandler,
    NoSuchAgent(Identity),
    UnconstructableType,
    AcceptAtomicMessage(Type),
    MissingPossession(Type),
    InvalidTraceForConstructorArguments,
    MessageError(MessageError),
    PossessionHandlerError(Box<dyn Error>),
    BuilderError(ParticipantMessageDescriptorBuilderError),
}

impl fmt::Display for ParticipantError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ParticipantError::*;

        match self {
            NonePossessionHandler => {
                write!(f, "action not permitted without possession handler")
            }
            NoSuchAgent(id) => write!(
                f,
                "no agent associated with id '{}' in architecture",
                id
            ),
            UnconstructableType => write!(f, "type is not constructable"),
            MissingPossession(_type) => {
                write!(f, "missing possession {:?} for constructor", _type)
            }
            InvalidTraceForConstructorArguments => {
                write!(f, "trace not valid given constructor arguments")
            }
            AcceptAtomicMessage(_type) => write!(
                f,
                "message of with term of type {} could not be accepted",
                serde_json::to_string(&_type).unwrap()
            ),
            MessageError(error) => write!(f, "message error, {}", error),
            PossessionHandlerError(error) => {
                write!(f, "possession handler error, {}", error)
            }
            BuilderError(error) => {
                write!(f, "build error, {}", error)
            }
        }
    }
}

impl Error for ParticipantError {}

impl From<MessageError> for ParticipantError {
    fn from(error: MessageError) -> ParticipantError {
        ParticipantError::MessageError(error)
    }
}

impl From<ParticipantMessageDescriptorBuilderError> for ParticipantError {
    fn from(
        error: ParticipantMessageDescriptorBuilderError,
    ) -> ParticipantError {
        ParticipantError::BuilderError(error)
    }
}
