use std::collections::HashSet;

use serde::{
    Deserialize,
    Serialize,
};

use crate::{
    participant::{
        message_builder::ParticipantMessageDescriptorBuilder,
        ParticipantError,
    },
    Architecture,
    Identity,
    Keyring,
    Message,
    PossessionHandler,
    Term,
    Type,
};

// -----------------------------------------------------------------------------
// Participant
// -----------------------------------------------------------------------------

/// A participant in a **ppc** system.
#[derive(Serialize, Deserialize)]
pub struct Participant<K, P>
where
    K: Keyring,
    P: PossessionHandler,
{
    architecture: Architecture,
    keyring: K,
    possession_handler: Option<P>,
}

impl<K, P> Participant<K, P>
where
    K: Keyring,
    P: PossessionHandler,
    P::Body: Clone,
{
    /// Initializes a new `Participant`.
    pub fn new(
        architecture: Architecture,
        keyring: K,
        possession_handler: Option<P>,
    ) -> Participant<K, P> {
        Participant {
            architecture,
            keyring,
            possession_handler,
        }
    }

    /// Returns a [`ParticipantMessageDescriptorBuilder`] that can be used to
    /// create a valid message.
    ///
    /// ## Example
    /// TODO:
    ///   This is one big example, would be nice to make it shorter. It's also
    ///   very vital for the usage of ppc-core so maybe it should be top level.
    /// ```
    /// # #[macro_use] extern crate ppc;
    /// use ppc::{
    /// #   participant::ParticipantError,
    ///     Term,
    ///     Identity,
    ///     KeyQuartette,
    ///     Type,
    ///     Architecture,
    ///     Participant,
    ///     Message,
    ///     Agent,
    /// };
    ///
    /// use std::collections::HashMap;
    ///
    /// # fn main() -> Result<(), ParticipantError> {
    /// // Create the architecture
    /// let mut architecture = Architecture::new();
    /// let id_b = architecture.insert_agent(agent!());
    /// let id_a = architecture.insert_agent(agent!(
    ///     => Type::atomic("QUOTE"),
    ///     Type::atomic("QUOTE") => Type::certificate(&id_b, "QUOTE")));
    ///
    /// let i_id_a = architecture.insert_input_interface(Agent::empty(id_a.clone()));
    /// let i_id_b = architecture.insert_input_interface(Agent::empty(id_b.clone()));
    /// let o_id_a = architecture.insert_output_interface(Agent::empty(id_a.clone()));
    /// let o_id_b = architecture.insert_output_interface(Agent::empty(id_b.clone()));
    ///
    /// let mut rng = rand::thread_rng();
    /// let keyring: HashMap<Identity, KeyQuartette> = vec![
    ///     (id_a.clone(), KeyQuartette::gen(&mut rng)),
    ///     (id_b.clone(), KeyQuartette::gen(&mut rng)),
    ///     (i_id_a.clone(), KeyQuartette::gen(&mut rng)),
    ///     (i_id_b.clone(), KeyQuartette::gen(&mut rng)),
    ///     (o_id_a.clone(), KeyQuartette::gen(&mut rng)),
    ///     (o_id_b.clone(), KeyQuartette::gen(&mut rng)),
    /// ]
    /// .into_iter()
    /// .collect();
    ///
    /// // Create the possesion handler.
    /// let possesion_handler: HashMap<Type, Vec<Message<Vec<u8>>>> =
    ///     HashMap::new();
    ///
    /// // Use the architecture, keyring and possession handler to create a
    /// // participant.
    /// let mut participant =
    ///     Participant::new(architecture, keyring, Some(possesion_handler));
    ///
    /// //
    /// // Now that we have a participant we can create messages with the
    /// // builder.
    /// //
    ///
    /// let data = "i think everybody should like everybody"
    ///     .as_bytes()
    ///     .to_vec();
    ///
    /// // Use the message builder to create a message with a term containing
    /// // the previously made data.
    /// let atomic = participant
    ///     .build_message()
    ///     .sender(id_a.clone())
    ///     .receiver(id_a.clone())
    ///     .term(Term::atomic(String::from("QUOTE"), data))
    ///     .sign()?;
    ///
    /// // Use the previously created message to create a certificate of it that
    /// // will be sent to the agent associated with `id_b`.
    /// let certificate = participant
    ///     .build_message()
    ///     .sender(id_a.clone())
    ///     .receiver(id_b.clone())
    ///     .argument(atomic)?
    ///     .certify(id_b.clone())
    ///     .sign()?;
    ///
    /// # Ok(())
    /// # }
    /// ```
    pub fn build_message(
        &mut self,
    ) -> ParticipantMessageDescriptorBuilder<K, P, Vec<u8>>
    where
        P::Body: From<Vec<u8>>,
    {
        ParticipantMessageDescriptorBuilder::new(self)
    }

    /// Creates a message. If the creation of the message fails (for any number
    /// of reasons) a `ParticipantError` is thrown.
    ///
    /// If the creation is successfull a copy of the newly created message will
    /// be added to the possession handler.
    pub fn create_message<T>(
        &mut self,
        sender: Identity,
        receiver: Identity,
        term: Term<T>,
    ) -> Result<Message<T>, ParticipantError>
    where
        T: Clone,
        P::Body: From<T>,
    {
        let message = Message::builder()
            .sender(sender)
            .receiver(receiver)
            .term(term)
            .sign(&self.keyring)?;
        attach_valid_trace(self, message)
    }

    /// Checks that a message and its trace is valid within the architecture of
    /// the system.
    pub fn validate<T>(
        &self,
        message: &Message<T>,
    ) -> Result<(), ParticipantError> {
        use ParticipantError::*;

        let sender = message.sender(&self.keyring)?;
        let term = message.term(&self.keyring)?;
        let _type = term._type();
        let trace = message.trace(&self.keyring)?;

        // Extract the types of the messages in the shallow trace. These should
        // match the arguments of any viable constructor.
        let trace_args: Result<HashSet<_>, _> = trace
            .iter()
            .map(|message| message.term(&self.keyring).map(Term::_type))
            .collect();
        let trace_args = trace_args?;

        // Check if any viable constructor has the same arguments as the trace.
        for constructor_args in self.viable_constructors(sender, _type)? {
            let constructor_args: HashSet<_> =
                constructor_args.into_iter().collect();

            if constructor_args == trace_args {
                for message in trace {
                    self.validate(message)?;
                }
                return Ok(());
            } else {
                return Err(InvalidTraceForConstructorArguments);
            }
        }

        Err(UnconstructableType)
    }

    fn viable_constructors(
        &self,
        id: &Identity,
        _type: &Type,
    ) -> Result<impl Iterator<Item = &Vec<Type>>, ParticipantError> {
        use ParticipantError::*;

        let (base_agent, input_agent, output_agent) = self
            .architecture
            .agent_borrow(id)
            .ok_or(NoSuchAgent(id.clone()))?;

        Ok(vec![
            base_agent.constructors_borrow().get(_type),
            input_agent.constructors_borrow().get(_type),
            output_agent.constructors_borrow().get(_type),
        ]
        .into_iter()
        // Turns [Option<T>] into [T] removing all None values.
        .filter_map(|x| x))
    }

    /// Verifies that a message containing a proof contains a valid proof.
    pub fn verify(
        &self,
        message: Message<Vec<u8>>,
    ) -> Result<(), ParticipantError> {
        Ok(message.into_term(&self.keyring)?.verify(&self.keyring)?)
    }

    /// Gain possesion over a received message.
    ///
    /// Messages with terms of atomic type can not be received since they are
    /// not meant to be sent outside of the base agent and its interfaces.
    ///
    /// ## NOTE:
    /// It is not necessary to call this function after the participant has
    /// created a message. It is only if the participant receives a message and
    /// needs possesion over it that this function should be called.
    pub fn accept(
        &mut self,
        message: Message<P::Body>,
    ) -> Result<(), ParticipantError> {
        use ParticipantError::*;

        let _type = message.term(&self.keyring)?._type();

        if _type.is_atomic() {
            Err(AcceptAtomicMessage(_type.clone()))
        } else {
            self.possession_handler
                .as_mut()
                .ok_or(NonePossessionHandler)?
                .insert_possession(&self.keyring, message)
                .map_err(|error| PossessionHandlerError(error.into()))
        }
    }

    /// Queries a message from the possesion handler of the participant.
    pub fn query_possession(
        &self,
        _type: &Type,
        receiver: &Identity,
    ) -> Result<Option<Message<P::Body>>, ParticipantError> {
        use ParticipantError::*;

        self.possession_handler
            .as_ref()
            .ok_or(NonePossessionHandler)?
            .query_possession(&self.keyring, _type, receiver)
            .map_err(|error| PossessionHandlerError(error.into()))
    }

    /// Gets the term of `message`
    pub fn extract_term<T>(
        &self,
        message: Message<T>,
    ) -> Result<Term<T>, ParticipantError> {
        Ok(message.into_term(&self.keyring)?)
    }

    /// Borrow the keyring of the participant.
    pub fn keyring(&self) -> &K { &self.keyring }

    pub fn architecture(&self) -> &Architecture { &self.architecture }
}

pub fn keyring<K, P>(participant: &Participant<K, P>) -> &K
where
    K: Keyring,
    P: PossessionHandler,
{
    &participant.keyring
}

pub fn attach_valid_trace<K, P, T>(
    participant: &mut Participant<K, P>,
    message: Message<T>,
) -> Result<Message<T>, ParticipantError>
where
    K: Keyring,
    P: PossessionHandler,
    T: Clone,
    P::Body: From<T>,
{
    use ParticipantError::*;

    let keyring = &participant.keyring;

    // Check that the receiver exists.
    let receiver = message.receiver(keyring)?.clone();
    participant
        .architecture
        .agent_borrow(&receiver)
        .ok_or(NoSuchAgent(receiver.clone()))?;

    let sender = message.sender(keyring)?.clone();
    let term = message.into_term(keyring)?;
    let _type = term._type();

    let constructor = participant
        .viable_constructors(&sender, _type)?
        .next()
        .ok_or(UnconstructableType)?
        // TODO:
        //   No big thing but this clone is only done to solve borrowing
        //   conflicts.
        .clone();

    let possession_handler = participant
        .possession_handler
        .as_mut()
        .ok_or(NonePossessionHandler)?;

    let mut message = Message::builder()
        .sender(sender.clone())
        .receiver(receiver)
        .term(term)
        .sign(keyring)?;

    // Attach possessions matching the arguments of the constructor to the
    // message.
    for _type in constructor.iter() {
        let possession = possession_handler
            .query_possession(&participant.keyring, _type, &sender)
            .map_err(|error| PossessionHandlerError(error.into()))?
            .ok_or(MissingPossession(_type.clone()))?
            .clone()
            .into_attachment();
        message.attach(&participant.keyring, possession)?;
    }

    // Gain possesion over the newly created message.
    possession_handler
        .insert_possession(
            &participant.keyring,
            message.clone().map(P::Body::from),
        )
        .map_err(|error| PossessionHandlerError(error.into()))?;

    Ok(message)
}

// -----------------------------------------------------------------------------
// Tests
// -----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use super::*;
    use crate::{
        Agent,
        KeyQuartette,
        Type,
    };

    #[test]
    fn create_message() {
        use ParticipantError::*;

        //
        // Setup.
        //

        let mut architecture = Architecture::new();

        let id_a = architecture.insert_agent(agent!(
            => Type::atomic("AROUND"),
            => Type::atomic("THE")
        ));
        let id_b = architecture.insert_agent(agent!(
            Type::atomic("AROUND"), Type::atomic("THE") => Type::atomic("WORLD")
        ));
        let i_id_a =
            architecture.insert_input_interface(Agent::empty(id_a.clone()));
        let i_id_b =
            architecture.insert_input_interface(Agent::empty(id_b.clone()));
        let o_id_a =
            architecture.insert_output_interface(Agent::empty(id_a.clone()));
        let o_id_b =
            architecture.insert_output_interface(Agent::empty(id_b.clone()));

        let mut rng = rand::thread_rng();
        let keyring: HashMap<Identity, KeyQuartette> = vec![
            (id_a.clone(), KeyQuartette::gen(&mut rng)),
            (id_b.clone(), KeyQuartette::gen(&mut rng)),
            (i_id_a.clone(), KeyQuartette::gen(&mut rng)),
            (i_id_b.clone(), KeyQuartette::gen(&mut rng)),
            (o_id_a.clone(), KeyQuartette::gen(&mut rng)),
            (o_id_b.clone(), KeyQuartette::gen(&mut rng)),
        ]
        .into_iter()
        .collect();

        let possession_handler: HashMap<Type, Vec<Message<&str>>> =
            HashMap::new();
        let mut participant =
            Participant::new(architecture, keyring, Some(possession_handler));

        // Create invliad message with type not in architecture.
        let invalid_message = participant.create_message(
            id_b.clone(),
            id_a.clone(),
            Term::atomic(String::from("DAFT"), "punk"),
        );
        assert!(matches!(invalid_message, Err(UnconstructableType)));

        // Create a message that is creatable but not currently since the agent
        // is missing required possessions.
        let invalid_message = participant.create_message(
            id_b.clone(),
            id_a.clone(),
            Term::atomic(String::from("WORLD"), "world"),
        );
        assert!(matches!(invalid_message, Err(MissingPossession(_))));

        //
        // Create valid messages and validate them.
        //

        let message = participant
            .create_message(
                id_a.clone(),
                id_b.clone(),
                Term::atomic(String::from("AROUND"), "around"),
            )
            .unwrap();
        participant.validate(&message).unwrap();

        let message = participant
            .create_message(
                id_a.clone(),
                id_b.clone(),
                Term::atomic(String::from("THE"), "the"),
            )
            .unwrap();
        participant.validate(&message).unwrap();

        let message = participant
            .create_message(
                id_b.clone(),
                id_a.clone(),
                Term::atomic(String::from("WORLD"), "world"),
            )
            .unwrap();
        participant.validate(&message).unwrap();
    }

    #[test]
    fn accept_message() {
        use ParticipantError::*;

        let mut architecture = Architecture::new();

        let id_a = architecture.insert_agent(agent!());
        let id_b = architecture.insert_agent(agent!(
            => Type::atomic("DERREZED"),
            Type::atomic("DERREZED") => Type::certificate(&id_a, "DERREZED")
        ));
        let i_id_a =
            architecture.insert_input_interface(Agent::empty(id_a.clone()));
        let i_id_b =
            architecture.insert_input_interface(Agent::empty(id_b.clone()));
        let o_id_a =
            architecture.insert_output_interface(Agent::empty(id_a.clone()));
        let o_id_b =
            architecture.insert_output_interface(Agent::empty(id_b.clone()));

        let mut rng = rand::thread_rng();
        let keyring: HashMap<Identity, KeyQuartette> = vec![
            (id_a.clone(), KeyQuartette::gen(&mut rng)),
            (id_b.clone(), KeyQuartette::gen(&mut rng)),
            (i_id_a.clone(), KeyQuartette::gen(&mut rng)),
            (i_id_b.clone(), KeyQuartette::gen(&mut rng)),
            (o_id_a.clone(), KeyQuartette::gen(&mut rng)),
            (o_id_b.clone(), KeyQuartette::gen(&mut rng)),
        ]
        .into_iter()
        .collect();

        let possession_handler: HashMap<Type, Vec<Message<Vec<u8>>>> =
            HashMap::new();

        let mut participant =
            Participant::new(architecture, keyring, Some(possession_handler));

        let message = participant
            .build_message()
            .sender(id_b.clone())
            .receiver(id_b.clone())
            .term(Term::atomic(
                String::from("DERREZED"),
                "tron: legacy".as_bytes().to_vec(),
            ))
            .sign()
            .unwrap();

        matches!(
            participant.accept(message.clone()).unwrap_err(),
            AcceptAtomicMessage(_)
        );

        let message = participant
            .build_message()
            .sender(id_b.clone())
            .receiver(id_a.clone())
            .argument(message)
            .unwrap()
            .certify(id_a.clone())
            .sign()
            .unwrap();

        participant.accept(message).unwrap();
    }
}
