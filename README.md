# ppc-core

A library for managing the provably private agent graph.

## Docs
[master](https://provablyprivate.gitlab.io/ppc-core/main/docs)
[staging](https://provablyprivate.gitlab.io/ppc-core/staging/docs)

## Use as a crate

ppc-core can be imported into a Rust project as a crate.
However, since it is a private git repository one has to make a few extra steps in order to import it.

### 1. Set up SSH key pair

Cargo (the Rust package manager) will download the repository using SSH.
In order to authenticate your access to the repository you will need to set up your public key with GitLab.
This page goes through the steps https://docs.gitlab.com/ee///ssh/README.html.

If you have previously added your public key to GitLab you can go directly to the next step.

### 2. Add the crate to Cargo.toml

Put this in your projects *Cargo.toml*.
```
[dependencies.ppc-core]
version = "0.1.0"
git = "ssh://git@gitlab.com/provablyprivate/ppc-core.git"
branch = "main" 
```

#### 2.1

If you are simultaneously working on ppc-core and a dependent program, you
can use a local directory as your dependency source.

```
[dependencies.ppc-core]
version = "0.1.0"
git = "/path/to/ppc-core
branch = "your-current-branch" 
```

### 3. Configure Cargo

In the projects root directory create a folder named *.cargo* and in that folder create a file named *config.toml*.
In the new file *.cargo/config.toml* put the following:
```
[net]
git-fetch-with-cli = true
```
This tells Cargo to use the systems **git** executable when fetching the crate.

### 4. Build

You should now be able to build your project running `cargo build`.

Here is an example of a repository that imports **ppc-core**, https://gitlab.com/provablyprivate/COPPA-MWP/-/tree/version-external-lib.
